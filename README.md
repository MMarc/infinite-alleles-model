# infinite-alleles-model

This repo contains code associated to the manuscript "The infinite alleles model revisited: a Gibbs sampling approach". It consists in Ocaml code for the Gibbs sampler, together with R code for post-processing analysis.

# Organization

The raw data should be put in the data folder.
Ocaml code can then be called to produce the simplified datasets (allele partitions) that will be put in the data-transformed folder.
Finally, results of MCMC and other simulations are produced by the ocaml code and are put in the simulation-outputs folder.
The content of this folder can further be read by the R code.

# Compiling

Use ocamlbuild to compile the ocaml code with the library "unix" like in the following example.

ocamlbuild -lib unix raw_to_datasets.native

./raw_to_datasets.native

Use the R code in an interactive R environment.
