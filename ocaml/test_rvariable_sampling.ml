open Rvariable
open Out

let test_distrib distrib n name =

  let rec one_more_sim recordlist = function
    |0 -> recordlist
    |i ->
      let simu = distrib () in
      let recordlist = simu::recordlist in
      one_more_sim recordlist (i-1) in
  
  let recordlist = one_more_sim [] n in
  let out_recordlist = open_out ("../simulation-outputs/test_rvariable_sampling/"^name^".csv") in
  output_string out_recordlist (string_of_list string_of_float "\n" recordlist)

let distrib_gig1 () =
  let lambda = 0. in
  let chi = 0.2 in
  let psi = 1.2 in
  simGIG lambda chi psi

let distrib_gamma1 () =
  let alpha = 10.5 in
  let beta = 2. in
  simGamma alpha beta

let _ = 
  test_distrib distrib_gig1 50000 "gig";
  test_distrib distrib_gamma1 50000 "gamma";
