open Nt
open Aa
open Allele
open Refseq
open List_addons

let string_of_bool = function
  |true -> "1"
  |false -> "0"

(** A string concatenating all elements of a list,
 * to which a function [string_of_element] is applied,
 * and with a separator [sep] in between elements. *)
let string_of_list string_of_element sep l =
  let rec aux buffer = function
  |[] -> buffer
  |hd::tl ->
      let new_buffer = if buffer = "" then string_of_element hd else buffer^sep^(string_of_element hd)
      in aux new_buffer tl
  in aux "" l

let id x = x

let string_of_ntmut = function
  |Ntsnp(i,nt) -> (String.make 1 refseq_uncut.[i])^(string_of_int (i+1))^(string_of_nt nt)
  |Ntdel(i,j) -> "DEL:"^(string_of_int (i+1))^":"^(string_of_int (j+1))
  |Ntins(i,ntl) -> "INS:"^(string_of_int (i+1))^":"^(string_of_list string_of_nt "" ntl)

let string_of_aamut = function
  |Aasnp(i,nt) -> (string_of_int i)^":"^(string_of_aa nt)
  |Aadel(i,j) -> "DEL"^(string_of_int i)^"-"^(string_of_int j)
  |Aains(i,aal) -> "INS"^(string_of_int i)^"-"^(string_of_list string_of_aa "" aal)

let string_of_nt_allele = string_of_list string_of_ntmut ";"
let string_of_aa_allele = string_of_list string_of_aamut ";"
let string_of_intlist = string_of_list string_of_int " "

let output_allele_members sep content l string_of_element =
  let out_alleles = open_out ("../data-transformed/"^content^".csv")
  in let out_names = open_out ("../data-transformed/"^content^"_names.csv")
  in let out_dates = open_out ("../data-transformed/"^content^"_dates.csv")
  in let out_regions = open_out ("../data-transformed/"^content^"_regions.csv")
  in let out_nb = open_out ("../data-transformed/"^content^"_nb.csv")

  in let rec writeOneMore list_of_alleles =
  match list_of_alleles with
  |[] -> ()
  |(all, lmetadata)::tl ->
      let i = List.length lmetadata in
      let lseqnames, lseqdates, lseqregions = split_triple lmetadata in
      begin
        output_string out_alleles ((string_of_element all)^"\n");
        output_string out_names ((string_of_list id sep lseqnames)^"\n");
        output_string out_names ((string_of_list id sep lseqdates)^"\n");
        output_string out_names ((string_of_list id sep lseqregions)^"\n");
        output_string out_nb ((string_of_int i)^"\n");
        writeOneMore tl
      end

  in begin
    writeOneMore l;
    close_out out_alleles;
    close_out out_names;
    close_out out_dates;
    close_out out_regions
  end

let output_allele_members2 sep content l output_fst =
  let out_alleles = open_out ("../data-transformed/"^content^".csv")

  in let rec writeOneMore list_of_alleles =
  match list_of_alleles with
  |[] -> ()
  |all::tl ->
      begin
        output_fst out_alleles all true;
        writeOneMore tl
      end

  in begin
    writeOneMore l;
    close_out out_alleles;
  end


(** Transforms into a string the content of listS or listN, either
 * the first line, if [first = true] or the second line otherwise. *)
let string_of_listx first listX = 
  let string_of_xt = function
    |(t,xt) -> if first then string_of_float t else string_of_float xt in
  string_of_list string_of_xt "," listX


(** Writes out a list of listS or listN, containing piecewise
 * values through time. It assumes that the time breaking points
 * are the same for all the listX, but that the values on each
 * interval differ.
 * The first line of the file contains the times of change.
 * All following lines contain values of the piecewise function. *)
let output_list_of_listx file l =
  begin
  (* we first write the list of times from the first listX in the record *)
  output_string file (string_of_listx true (List.hd l));
  output_string file "\n";
  (* then the X values *)
  output_string file (string_of_listx false (List.hd l));
  output_string file "\n";
  (* and then only the X values for all other lists *)
  output_string file (string_of_list (string_of_listx false) "\n" (List.tl l));
  end


(** String representing a lineage [(t,n)]. *)
let string_of_lineage (t,n) =
  "(" ^ string_of_float t ^ "," ^ string_of_int n ^ ")"
let string_of_lineage_v2 (t,n,action) =
  "(" ^ string_of_float t ^ "," ^ string_of_int n ^ "," ^ string_of_int action ^ ")"
(** String representing a list of lineages. *)
let string_of_history = string_of_list string_of_lineage ";" 
let string_of_history_v2 = string_of_list string_of_lineage_v2 ";" 
let string_of_lineagelist = string_of_list string_of_float ";" 
(** String representing an allele, i.e. a list of lists of lineages. *)
let string_of_alleles = string_of_list string_of_lineagelist " | " 

(* note that we revert the list to get the individuals in increasing order,
 * and thus decreasing sampling time order. *)
let string_of_samp_history samp_history = 
  let string_of_element (t,_) = string_of_float t in
  string_of_list (string_of_element) "," (List.rev samp_history)

(* note that we first order the coal_history according to the
 * increasing individual number to get something standardized. *)
let string_of_coal_history bool_times coal_history =
  let custom_compare (_,n1,_) (_,n2,_) = compare n1 n2 in
  let string_of_action (_,_,a) = string_of_int a in
  let string_of_date (t,_,_) = string_of_float t in
  let coal_history = List.sort custom_compare coal_history in
  if bool_times then
    string_of_list (string_of_date) "," coal_history
  else
    string_of_list (string_of_action) "," coal_history

(** writes out the hyperprior used in an analysis, in three files:
 * - prior_N.csv: first line corresponds to changing times,
 *      second line corresponds to lambda values on the different intervals
 *      third line corresponds to chi values on different intervals
 *      fourth line corresponds to psi values on different intervals
 * - prior_S.csv: first line corresponds to changing times,
 *      second line corresponds to alpha values on different intervals
 *      third line corresponds to beta values on different intervals
 * - prior_mu.csv: first line: alpha, second line: beta. *)
let output_prior hyperpriors =    
  let hyperprior_N, hyperprior_S, hyperprior_mu, hyperprior_g, tor = hyperpriors in
  let (lambda, chi, psi) = hyperprior_N in
  let (alpha_S, beta_S) = hyperprior_S in
  let (alpha_mu, beta_mu) = hyperprior_mu in
  let (alpha_g, beta_g) = hyperprior_g in

  let out_prior_N = open_out ("prior_N.csv") in
  let out_prior_S = open_out ("prior_S.csv") in
  let out_prior_mu = open_out ("prior_mu.csv") in
  let out_prior_g = open_out ("prior_g.csv") in
  let out_prior_tor = open_out ("prior_tor.csv") in

  begin
    output_string out_prior_N ((string_of_float lambda)^"\n");
    output_string out_prior_N ((string_of_float chi)^"\n");
    output_string out_prior_N ((string_of_float psi)^"\n");

    output_string out_prior_S ((string_of_float alpha_S)^"\n");
    output_string out_prior_S ((string_of_float beta_S)^"\n");

    output_string out_prior_mu ((string_of_float alpha_mu)^"\n");
    output_string out_prior_mu ((string_of_float beta_mu)^"\n");

    output_string out_prior_g ((string_of_float alpha_g)^"\n");
    output_string out_prior_g ((string_of_float beta_g)^"\n");

    output_string out_prior_tor ((string_of_float tor)^"\n");

    close_out out_prior_N;
    close_out out_prior_S;
    close_out out_prior_mu;
    close_out out_prior_g;
    close_out out_prior_tor
  end

(** writes out the data used as input in an analysis, in two files:
 * - truth_sampling_times.csv: a unique line with comma-separated floats.
 * - truth_alleles.csv: a unique line with comma separated ints
 *       corresponding to the allele number of individuals in the same
 *       order as in the first file. *)
let output_sampling_truth array_individuals =
  let allele_membership_list =
    let get_allele (_, _, _, ai) = ai in
    List.map get_allele (Array.to_list array_individuals) in
  
  let birth_time_list =
    let get_birth (bi, _, _, _) = bi in
    List.map get_birth (Array.to_list array_individuals) in

  let out_truth_alleles = open_out ("truth_alleles.csv") in
  let out_truth_sampling_times = open_out ("truth_sampling_times.csv") in
  begin
    output_string out_truth_alleles ((string_of_list string_of_int "," allele_membership_list)^"\n");
    output_string out_truth_sampling_times ((string_of_list string_of_float "," birth_time_list)^"\n");
    close_out out_truth_alleles;
    close_out out_truth_sampling_times
  end

(** writes out hidden truth about a dataset, that one might know
 * if, e.g. the data has been simulated. In four files:
 * - truth_N.csv: first line for times of change, second lines for values.
 * - truth_S.csv: first line for times of change, second lines for values.
 * - truth_mu.csv: a unique line with the true value.
 * - truth_coal_times.csv: a unique line of comma-separated death times
 *      for individuals ordered as usual.
 * - truth_coal_actions.csv: a unique file of 0/1 specifying whether the
 *      death corresponds to a mutation (1) or coalescence (0). *)
let output_hidden_truth listN listS mu g coal_history =
  
  let out_truth_N = open_out ("truth_N.csv") in
  let out_truth_S = open_out ("truth_S.csv") in
  let out_truth_mu = open_out ("truth_mu.csv") in
  let out_truth_g = open_out ("truth_g.csv") in
  let out_truth_coal_times = open_out ("truth_coal_times.csv") in
  let out_truth_coal_actions = open_out ("truth_coal_actions.csv") in

  begin
    output_list_of_listx out_truth_N (listN::[]);
    output_list_of_listx out_truth_S (listS::[]);
    output_string out_truth_mu ((string_of_float mu)^"\n");
    output_string out_truth_g ((string_of_float g)^"\n");
    output_string out_truth_coal_times ((string_of_coal_history true coal_history)^"\n");
    output_string out_truth_coal_actions ((string_of_coal_history false coal_history)^"\n");

    close_out out_truth_N;
    close_out out_truth_S;
    close_out out_truth_mu;
    close_out out_truth_g;
    close_out out_truth_coal_times;
    close_out out_truth_coal_actions
  end

(** writes out the posterior distributions sampled during an analysis, in 4 files:
 * - posterior_N.csv: first line for times of change, following lines for values sampled throughout the mcmc.
 * - posterior_S.csv: first line for times of change, following lines for values sampled throughout the mcmc.
 * - posterior_mu.csv: different lines for values sampled throughout the mcmc.
 * - posterior_coal_times.csv: each line corresponds to death times of individuals ordered as usual.
 *       Different lines for different time-steps in the mcmc.
 * - posterior_coal_actions.csv: each line corresponds to 1 (mutations) or 0 (coalescences)
 *       of individuals ordered as usual.
 *       Different lines for different time-steps in the mcmc.*)
let output_gibbs_result posterior_coal_history posterior_listN posterior_listS posterior_mu =

  let out_posterior_mu = open_out ("posterior_mu.csv") in
  let out_posterior_S = open_out ("posterior_S.csv") in
  let out_posterior_N = open_out ("posterior_N.csv") in
  let out_posterior_coal_times = open_out ("posterior_coal_times.csv") in
  let out_posterior_coal_actions = open_out ("posterior_coal_actions.csv") in

  begin
    output_string out_posterior_mu (string_of_list string_of_float "\n" posterior_mu);
    output_list_of_listx out_posterior_S posterior_listS;
    output_list_of_listx out_posterior_N posterior_listN;
    output_string out_posterior_coal_times (string_of_list (string_of_coal_history true) "\n" posterior_coal_history);
    output_string out_posterior_coal_actions (string_of_list (string_of_coal_history false) "\n" posterior_coal_history);

    close_out out_posterior_mu;
    close_out out_posterior_S;
    close_out out_posterior_N;
    close_out out_posterior_coal_times;
    close_out out_posterior_coal_actions
  end


(** Writing out a record of mu, listS and listN on
 * a file, potentially with a nice header if [with_header]. *)
let write_out_params listN listS mu with_header file =
  let rec aux header params letter i = function
    |[] -> header, params
    |(t,x)::tl ->
        let new_header = (letter^(string_of_int i))::header in
        let new_params = x::params in
        aux new_header new_params letter (i+1) tl in
  let header, params = "mu"::[], mu::[] in
  let header, params = aux header params "N" 0 listN in
  let header, params = aux header params "S" 0 listS in
  let header = List.rev header in
  let params = List.rev params in
  if with_header then 
    output_string file ((string_of_list id ";" header)^"\n");
  output_string file ((string_of_list string_of_float ";" params)^"\n")


(** Writing out a record of many mu's, listS's and listN's
 * on one file, with a header. *)
let write_out_many_params list_listN list_listS list_mu file =
  let rec aux i = function
    |[], [], [] -> ()
    |listN::tl_listN, listS::tl_listS, mu::tl_mu ->
        begin
        write_out_params listN listS mu (i=0) file;
        aux (i+1) (tl_listN, tl_listS, tl_mu)
        end
    |_, _, _ -> failwith "We only want to write lists of parameters of same size here." in
  aux 0 (list_listN, list_listS, list_mu)


