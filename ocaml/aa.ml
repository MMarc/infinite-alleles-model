open Nt

type aa = Phe | Leu | Ile | Met | Val | Ser | Pro | Thr | Ala | Tyr | His | Gln | Asn | Lys | Asp | Glu | Cys | Trp | Arg | Gly | Unknown | Stop

(** is the amino-acid coresponding to a given [codon], which is a triplet of nucleotides. *)
let aa_of_codon codon =
  match codon with
  |(T,T,T) -> Phe
  |(T,T,C) -> Phe
  |(T,T,A) -> Leu
  |(T,T,G) -> Leu

  |(T,C,_) -> Ser

  |(T,A,T) -> Tyr
  |(T,A,C) -> Tyr
  |(T,A,A) -> Stop
  |(T,A,G) -> Stop

  |(T,G,T) -> Cys
  |(T,G,C) -> Cys
  |(T,G,A) -> Stop
  |(T,G,G) -> Trp

  |(C,T,_) -> Leu

  |(C,C,_) -> Pro

  |(C,A,T) -> His
  |(C,A,C) -> His
  |(C,A,A) -> Gln
  |(C,A,G) -> Gln

  |(C,G,_) -> Arg

  |(A,T,T) -> Ile
  |(A,T,C) -> Ile
  |(A,T,A) -> Ile
  |(A,T,G) -> Met

  |(A,C,_) -> Thr

  |(A,A,T) -> Asn
  |(A,A,C) -> Asn
  |(A,A,A) -> Lys
  |(A,A,G) -> Lys

  |(A,G,T) -> Ser
  |(A,G,C) -> Ser
  |(A,G,A) -> Arg
  |(A,G,G) -> Arg

  |(G,T,_) -> Val

  |(G,C,_) -> Ala

  |(G,A,T) -> Asp
  |(G,A,C) -> Asp
  |(G,A,A) -> Glu
  |(G,A,G) -> Glu

  |(G,G,_) -> Gly

  |(_,_,_) -> Unknown

(** a standard char associated to an amino-acid [aa]. *)
let char_of_aa aa =
  match aa with
  |Phe -> 'F'
  |Leu -> 'L'
  |Ile -> 'I'
  |Met -> 'M'
  |Val -> 'V'
  |Ser -> 'S'
  |Pro -> 'P'
  |Thr -> 'T'
  |Ala -> 'A'
  |Tyr -> 'Y'
  |His -> 'H'
  |Gln -> 'Q'
  |Asn -> 'N'
  |Lys -> 'K'
  |Asp -> 'D'
  |Glu -> 'E'
  |Cys -> 'C'
  |Trp -> 'W'
  |Arg -> 'R'
  |Gly -> 'G'
  |Unknown -> 'X'
  |Stop -> 'Z'

(** A string of length 1 with the character of a given amino-acid [aa]. *)
let string_of_aa aa =
  String.make 1 (char_of_aa aa)
 
(** An int corresponding to an amino-acid [aa]. Note that this is by no means standard ! *)
let int_of_aa aa =
  match aa with
  |Phe -> 0
  |Leu -> 1
  |Ile -> 2
  |Met -> 3
  |Val -> 4
  |Ser -> 5
  |Pro -> 6
  |Thr -> 7
  |Ala -> 8
  |Tyr -> 9
  |His -> 10
  |Gln -> 11
  |Asn -> 12
  |Lys -> 13
  |Asp -> 14
  |Glu -> 15
  |Cys -> 16
  |Trp -> 17
  |Arg -> 18
  |Gly -> 19
  |Unknown -> 20
  |Stop -> 21

(** Comparison function of two aa.
 * Relies on the transformation of an aa into an int. *)
let compare_aa aa1 aa2 =
  if aa1 = aa2 then 0
  else
    if int_of_aa aa1 > int_of_aa aa2 then 1
    else -1
