exception Out_of_range

(**********************************
 * Basic random numbers
 * ********************************)


(** An integer distributed according to a Poisson
 * distribution with parameter [alpha]. *)
let simPoisson alpha = 
  if alpha <= 0. then 
    raise Out_of_range
  else
    let k = ref 0 in 
    let produit = ref (Random.float 1.0) in 
    while !produit >= exp (-. alpha) do 
      k := !k + 1;
      produit := !produit *. (Random.float 1.0);
    done;
    !k


(** A floating point number distributed according to an exponential 
 * distribution with rate [lambda]. *)
let simExp lambda =
  if lambda <= 0. then 
    raise Out_of_range
  else
    let u = Random.float 1.0
    in -. (log u) /. lambda


(** A floating point number distributed according to an exponential
 * distribution with rate [lambda] conditioned on falling on the
 * interval (tstart, tend). *)
let simExp_cond_interval lambda tstart tend =
  if lambda <= 0. then 
    raise Out_of_range
  else
    let u = Random.float 1.0 in
    let aux t = exp (-. lambda *. t) in
    (*-. log ( (aux tstart) -. u *. ((aux tstart) -. (aux tend)) ) /. lambda *)
(* alternatively, same formula, but with only one exponential: could it be faster ? *)
    tstart -. 1. /. lambda *. log ( 1. -. u *. (1. -. (aux (tend -. tstart) ) ) )


(** A floating point number distributed according to a Gaussian law 
 * with mean [mu] and standard error [sigma]. *)
let simGauss mu sigma =
  let r = simExp 0.5
  in let pi = 4. *. atan 1.
  in let theta = 2. *. pi *. Random.float 1.
  in let x = (sqrt r) *. (cos theta)
  in sigma *. x +. mu


(** A floating point number distributed according to a Gamma
 * random variable with shape [alpha] being an integer,
 * and rate beta = 1. *)
let simGamma_alpha_int alpha = 
  let rec aux sum = function
    |0 -> sum
    |alpha -> aux (sum +. simExp 1.) (alpha-1)
  in aux 0. alpha


(** A floating point number distributed according to a Gamma
 * random variable with shape [alpha] being a float in ]0,1[,
 * and rate beta = 1, using Ahrens-Dieter acceptance–rejection method. *)
let simGamma_AD alpha = 
  (* the int truncation and fractional part of alpha *)
  let k = int_of_float alpha in
  let delta = alpha -. float_of_int k in

  (* to simulate the Gamma(delta, 1.) part *)
  let lim_u = exp 1. /. ((exp 1.) +. delta) in
  let rec aux () =
    let u = Random.float 1. in
    let v = Random.float 1. in
    let w = Random.float 1. in
    let ksi, eta =
      if u <= lim_u then 
        let ksi = Float.pow v (1./.delta) in
        ksi, w *. Float.pow ksi (delta -. 1.)
      else
        let ksi = 1. -. log v in
        ksi, w *. exp (-. ksi) in
    if eta <= (Float.pow ksi (delta -. 1.)) *. exp (-. ksi) then ksi
    else aux () in

  if alpha <= 0. then 
    failwith "Gamma variable with shape alpha <= 0 is not defined."
  else
    (* the result is the sum of Gamma(k,1) and Gamma(delta,1) *)
    (simGamma_alpha_int k) +. aux ()


(** A floating point number distributed according to a Gamma
 * random variable with shape [alpha] being a float, preferentially > 1,
 * and rate beta = 1, using Marsaglia's rejection method. *)
let simGamma_Marsaglia alpha =

  let sim_alpha_sup1 alpha =
    let d = alpha -. 1./.3. in
    let c = 1. /. sqrt (9. *. d) in
    let rec aux () =
      let x = simGauss 0. 1. in
      let u = Random.float 1. in
      let v = (1. +. c *. x) *. (1. +. c *. x) *. (1. +. c *. x) in
      if v > 0. && log u < x *. x /. 2. +. d -. d *. v +. d *. log v then 
        d *. v
      else
        aux () in
    aux () in

  if alpha <= 0. then 
    failwith "Gamma variable with shape alpha <= 0 is not defined."
  else if alpha < 1. then 
    let u = Random.float 1. in
    (sim_alpha_sup1 (alpha +. 1.)) *. Float.pow u (1./.alpha)
  else 
    sim_alpha_sup1 alpha


(** A floating point number distributed according to a Gamma
 * random variable with shape [alpha] and rate [beta]. *)
let simGamma shape rate =
  let simGamma1 shape =

    if shape = 1. then simExp 1.

    else if shape < 1. then
      let rec aux1 () =
        let u = Random.float 1. in
        let v = simExp 1. in
        if u <= 1. -. shape then
          let x = Float.pow u (1./.shape) in
          if x <= v then x
          else aux1 ()
        else
          let y = -. log ((1. -. u) /. shape) in
          let x = Float.pow (1. -. shape +. shape *. y) (1. /. shape) in
          if x <= v +. y then x
          else aux1 () in
      aux1 ()

    else
      let b = shape -. 1. /. 3. in
      let c = 1. /. sqrt (9. *. b) in
     
      let rec aux2 () =
        let rec draw_x_v () =
          let x = simGauss 0. 1. in
          let v = 1.0 +. c *. x in
          if v <= 0. then draw_x_v ()
          else x, v in
        let x, v = draw_x_v () in
        let v = v *. v *. v in
        let u = Random.float 1. in
        if u < 1.0 -. 0.0331 *. (x*.x) *. (x*.x) then b *. v
        else if log u < 0.5 *. x *. x +. b *. (1. -. v +. log v) then b *. v
        else aux2 () in
      aux2 () in

  if rate <= 0. then
    begin
      Printf.printf "shape = %f, rate = %f\n" shape rate;      
      failwith "Gamma variable with rate <= 0 is not defined."
    end
  else
    (simGamma1 shape) /. rate


(** A floating point number distributed according to an Inverse Gamma
 * distribution with shape [alpha] and scale [beta], i.e. with pdf proportional to
 * x^{-(shape + 1)} exp (-beta x^{-1}). *)
let simInvGamma shape scale =
  let x = simGamma shape scale in
  1. /. x


(** The sum of all integers in an int array [arr]. *)
let sum_arr arr =
  Array.fold_left (+) 0 arr


(** An integer distributed according to the integer weights in the
 * array [arr]. *)
let sample_one_group arr =
  let total_weight = sum_arr arr
  in let index = Random.int total_weight
  in let cumsum = ref (-1)
  in let i = ref (-1)
  in begin
    while !cumsum < index do
      i := !i + 1;
      cumsum := !cumsum + arr.(!i)
    done;
    !i
  end


(** A tuple of two integers sampled in [0,max[ 
  * without replacement. *)
let sample_two_without_replacement max =
  let k = Random.int max in
  let kprime = 
    let k2 = ref k in
    while k = !k2 do
      k2 := Random.int max
    done;
    !k2 in
  k, kprime


(** A floating point number distributed according to an IG:
  * Inverse Gaussian law, with parameters mu, lambda, where
  * the natural parameters are -lambda/(2mu^2) and -lambda/2. *)
let simIG mu lambda =
  let nu = simGauss 0. 1. in
  let y = nu*.nu in
  let x = mu +. mu *. mu *. y /. (2. *. lambda) -. mu *. sqrt( 4. *. mu *. lambda *. y +. mu *. mu *. y *. y ) /. (2. *. lambda) in
  let z = Random.float 1. in
  if z <= mu /. (mu +. x) then x
  else mu *. mu /. x



(** Compute the mode of the GIG distribution with
 * parameters [lambda] and [omega]. *)
let gig_mode lambda omega =
  if lambda >= 1. then
    (* mode of fgig(x) *)
    (sqrt ( (lambda -. 1.)*.(lambda -. 1.) +. omega *. omega ) +. (lambda -. 1.)) /. omega
  else
    (* 0 <= lambda < 1: use mode of f(1/x) *)
    omega /. ( sqrt ((1. -. lambda) *. (1. -. lambda) +. omega*.omega) +. (1. -. lambda) )


(** Simulation of a random variable following a GIG(lambda, chi, psi)
 * distribution, using the Type 1 method: Ratio-of-uniforms without shift.
 *   Dagpunar (1988), Sect.~4.6.2
 *   Lehner (1989) *)
let rgig_ROU_noshift lambda lambda_old omega alpha =

  (* shortcuts *)
  let t = 0.5 *. (lambda -. 1.) in
  let s = 0.25 *. omega in
  
  (* mode = location of maximum of sqrt(f(x)) *)
  let xm = gig_mode lambda omega in

  (* normalization constant: c = log(sqrt(f(xm))) *)
  let nc = t *. log (xm) -. s *. (xm +. 1./.xm) in

  (* location of maximum of x*sqrt(f(x)):
      we need the positive root of
      omega/2*y^2 - (lambda+1)*y - omega/2 = 0 *)
  let ym = ((lambda +. 1.) +. sqrt ( (lambda +. 1.)*.(lambda +. 1.) +. omega*.omega )) /. omega in

  (* boundaries of minmal bounding rectangle:
   * we us the "normalized" density f(x) / f(xm). hence
   * upper boundary: vmax = 1.
   * left hand boundary: umin = 0.
   * right hand boundary: umax = ym * sqrt(f(ym)) / sqrt(f(xm)) *)
  let um = exp (0.5 *. (lambda+.1.) *. log (ym) -. s *. (ym +. 1./.ym) -. nc) in

  (* Generate sample by acceptance-rejection *)
  let rec aux () =
    let u = Random.float um in
    let v = Random.float 1. in
    let x = u /. v in
    if log v > t *. (log x) -. s *. (x +. 1. /. x) -. nc then aux ()
    else
      if lambda_old < 0. then alpha /. x
      else alpha *. x in
  aux ()


(** Simulation of a random variable following a GIG(lambda, chi, psi)
 * distribution, using the Type 4:
 * New approach, constant hat in log-concave part.
 * Case: 0 < lambda < 1, 0 < omega < 1 . *)
let rgig_newapproach1 lambda lambda_old omega alpha =
  if lambda >= 1. || omega >1. then
    failwith "Invalid parameters within the rgig_newapproach1 function."
  else
    (* mode = location of maximum of sqrt(f(x)) *)
    let xm = gig_mode lambda omega in

    (* splitting point *)
    let x0 = omega /. (1.-.lambda) in

    (* domain [0, x_0] *)
    let k0 = exp ((lambda-.1.) *. log(xm) -. 0.5 *. omega *. (xm +. 1./.xm)) in
    let a0 = k0 *. x0 in

    let a1, a2, k1, k2 =
      (* domain [x_0, Infinity] *)
      if x0 >= 2. /. omega then
        let k1 = 0. in
        let a1 = 0. in
        let k2 = Float.pow x0 (lambda -. 1.) in
        let a2 = k2 *. 2. *. exp (-. omega *. x0 /. 2.) /. omega in
        a1, a2, k1, k2
      else
        (* domain [x_0, 2/omega] *)
        let k1 = exp (-. omega) in
        let a1 =
          if lambda = 0. then 
            k1 *. log (2. /. (omega *. omega))
          else
            k1 /. lambda *. ( (Float.pow (2. /. omega) lambda) -. (Float.pow x0 lambda) ) in

        (* domain [2/omega, Infinity] *)
        let k2 = Float.pow (2. /. omega) (lambda -. 1.) in
        let a2 = k2 *. 2. *. exp (-.1.) /. omega in
        a1, a2, k1, k2 in

    (* total area *)
    let atot = a0 +. a1 +. a2 in

    let rec aux () =
      let v, x, hx =
        let v = Random.float atot in
        if v <= a0 then
          let x = x0 *. v /. a0 in
          let hx = k0 in
          v, x, hx
        else 
          let v = -. a0 in
          if v <= a1 then
            if lambda = 0. then
              let x = omega *. exp( v *. exp (omega) ) in
              let hx = k1 /. x in
              v, x, hx
            else
              let x = Float.pow ( (Float.pow x0 lambda) +. (lambda /. k1 *. v) ) (1. /. lambda) in
              let hx = k1 *. Float.pow x (lambda -. 1.) in
              v, x, hx
          else
            let v = -. a1 in
            let a = if x0 > 2. /. omega then x0 else 2. /. omega in
            let x = -. 2. /. omega *. log ( exp (-. omega /. 2. *. a) -. omega /. (2. *. k2) *. v ) in
            let hx = k2 *. exp (-. omega /. 2. *. x) in
            v, x, hx in
      let u = Random.float hx in
      if log u <= (lambda -. 1.) *. (log x) -. omega /. 2. *. (x +. 1./.x) then
        if lambda_old < 0. then alpha /. x
        else alpha *. x
      else
        aux () in
    aux ()

    
(** Simulation of a random variable following a GIG(lambda, chi, psi)
 * distribution, using the Type 8:
 * Ratio-of-uniforms with shift by 'mode', alternative implementation.
 *   Dagpunar (1989)
 *   Lehner (1989) *)
let rgig_ROU_shift_alt lambda lambda_old omega alpha =
  (* shortcuts *)
  let t = 0.5 *. (lambda -. 1.) in
  let s = 0.25 *. omega in

  (* mode = location of maximum of sqrt(f(x)) *)
  let xm = gig_mode lambda omega in

  (* normalization constant: c = log(sqrt(f(xm))) *)
  let nc = t *. (log xm) -. s *. (xm +. 1./.xm) in

  (* compute coeffients of cubic equation y^3+a*y^2+b*y+c=0 *)
  let a = -. ( 2. *. (lambda +. 1.) /. omega +. xm ) in
  let b = 2. *. (lambda -. 1.) *. xm /. omega -. 1. in
  let c = xm in

  (* substitute y=z-a/3 for depressed cubic equation z^3+p*z+q=0 *)
  let p = b -. a*.a /. 3. in
  let q = (2. *. a *. a *. a) /. 27. -. (a *. b) /. 3. +. c in

  (* use Cardano's rule *)
  let fi = acos (-. q /. (2. *. sqrt (-. (p *. p *. p) /. 27.))) in
  let fak = 2. *. sqrt (-. p /. 3.) in
  let y1 = fak *. cos (fi /. 3.) -. a /. 3. in
  let y2 = fak *. cos (fi /. 3. +. 4. /. 3. *. Float.pi) -. a /. 3. in

  (* boundaries of minmal bounding rectangle:
   * we us the "normalized" density f(x) / f(xm). hence
   * upper boundary: vmax = 1. 
   * left hand boundary: uminus = (y2-xm) * sqrt(f(y2)) / sqrt(f(xm))
   * right hand boundary: uplus = (y1-xm) * sqrt(f(y1)) / sqrt(f(xm)) *)
  let uplus  = (y1 -. xm) *. exp (t *. (log y1) -. s *. (y1 +. 1. /. y1) -. nc) in
  let uminus = (y2 -. xm) *. exp (t *. (log y2) -. s *. (y2 +. 1. /. y2) -. nc) in

  let rec aux () =
    let u = uminus +. Random.float (uplus -. uminus) in
    let v = Random.float 1. in
    let x = u /. v +. xm in
    (* Acceptance/Rejection *)
    if x <= 0. || log v > t *. (log x) -. s *. (x +. 1. /. x) -. nc then aux ()
    else 
      if lambda_old < 0. then alpha /. x
      else alpha *. x in
  aux ()


let simGIG lambda chi psi =

  if  lambda = infinity || 
      chi = infinity || 
      psi = infinity || 
      chi < 0. || 
      psi < 0. ||
      (chi = 0. && lambda <= 0.) ||
      (psi = 0. && lambda >= 0.) then
    begin
      Printf.printf "lambda = %f, chi = %f, psi = %f\n" lambda chi psi;
      failwith "Invalid argument for the GIG distribution." 
    end

  else if chi < Float.epsilon then
    (* special cases which are basically Gamma and Inverse Gamma distribution *)
    if lambda > 0. then simGamma lambda (psi /. 2.)
    else 1. /. ( simGamma (-.lambda) (psi /. 2.) )

  else if  psi < Float.epsilon then
    (* special cases which are basically Gamma and Inverse Gamma distribution *)
    if lambda > 0. then 1. /. ( simGamma lambda (chi/.2.) )
    else simGamma (-.lambda) (chi/.2.)

  else
    let lambda_old = lambda in
    let lambda = if lambda < 0. then -. lambda else lambda in
    let alpha = sqrt (chi /. psi) in
    let omega = sqrt (psi *. chi) in

    (* run generator *)
    if lambda > 2. || omega > 3. then
      (* Ratio-of-uniforms with shift by 'mode', alternative implementation *)
      rgig_ROU_shift_alt lambda lambda_old omega alpha

    else if lambda >= 1. -. 2.25 *. omega *. omega || omega > 0.2 then
      (* Ratio-of-uniforms without shift *)
      rgig_ROU_noshift lambda lambda_old omega alpha

    else if lambda >= 0. && omega > 0. then
      (* New approach, constant hat in log-concave part. *)
      rgig_newapproach1 lambda lambda_old omega alpha
     
    else
      failwith "parameters of the GIG must satisfy lambda >= 0 and omega > 0."




