open Nt
open Allele
open Out
open In
open Refseq
open Date

(* we get rid of annoying aaaa at the end of the seq
 * and cut the beginning to ensure that more sequences
 * can be compared *)
let refseq = 
  let first_pos_included_for_comparison = 250 in
  let first_pos_excluded_for_comparison = 29700 in
  let comparison_length = first_pos_excluded_for_comparison - first_pos_included_for_comparison in
  String.sub refseq_uncut first_pos_included_for_comparison comparison_length

(** Computes the allele partition for all sequences in a fasta
 * file, where the corresponding information in the metadata file
 * must come from a given region and having been sequenced before
 * a given date. *)
let collect_allele_partition fasta_file metadata_info date_max =

  (* analyze one seq attached to a seqdate and use it to update 
   * a record of counts "counting" and a record of alleles "record" *)
  let update seqdate seq counting record =

    let (list_of_refseq, list_of_alleles, processed) = record in
    let (incorrect_date, too_recent, prop_n, gap, start_not_found, too_small, ok) = counting in

    (* we process the sequence and update the counters and allele partition *)
    let new_counting, new_record =

      (* first check that we have a date, and that it's below our maximal date *)
      if not(is_correct_date seqdate) then 
        (incorrect_date+1, too_recent, prop_n, gap, start_not_found, too_small, ok),
        record

      else
        let date = date_of_string seqdate in
        if date > date_max then 
          begin
            Printf.printf "too recent:%s\n" seqdate;
            (incorrect_date, too_recent+1, prop_n, gap, start_not_found, too_small, ok),
            record
          end

        else
          (* we further check that the quality is ok, using the proportion of n *)
          let prop_of_n = prop_of_undefined_nt seq in
          if prop_of_n > 0.1 then
              (incorrect_date, too_recent, prop_n+1, gap, start_not_found, too_small, ok),
              record

          else

            (* we want to be sure that all our sequences start at the same place, 
             * so we check against the "master reference" *)
            let tol = 2. in
            let score_imperfect = 0.8 in
            let primer = String.sub refseq 0 30 in
            let subseq = String.sub seq 0 500 in
            match find_primer subseq primer tol score_imperfect with
            |exception StartNotFound ->
              (incorrect_date, too_recent, prop_n, gap, start_not_found+1, too_small, ok),
              record

            |shift ->
                if (String.length refseq) + shift > String.length seq then
                  (incorrect_date, too_recent, prop_n, gap, start_not_found, too_small+1, ok),
                  record

                else
                  (* we can now cut the sequence and remove the unwanted initial and final nucleotides *)
                  let seq = String.sub seq shift (String.length refseq) in

                  (* if the snp list is greater than the limit we put, we interpret it as a gap.
                   * we thus compare it to the next reference in the list of reference sequences,
                   * until finding a snp list of correct size.
                   * If we don't, we record the sequence . *)
                  let limit_size = 100 in
                  let rec get_compressed_representation i = function
                    |[] -> true, (i, [])
                    |ref::tl ->
                        let snp_list = get_snp_list ref seq 0 limit_size in
                        if List.length snp_list = limit_size then 
                          get_compressed_representation (i+1) tl
                        else
                          false, (i, snp_list) in
                  let is_new, compressed_representation = get_compressed_representation 0 list_of_refseq in

                  let new_list_of_refseq =
                    if is_new then List.append list_of_refseq (seq::[])
                    else list_of_refseq in

                  let new_list_of_alleles = insert_in_compressed_representation_alleles seqdate compressed_representation list_of_alleles in
                  let new_processed = seqdate::processed in
                  (incorrect_date, too_recent, prop_n, gap, start_not_found, too_small, ok+1),
                  (new_list_of_refseq, new_list_of_alleles, new_processed) in

    new_counting, new_record in

  (* read one line after the other in the fasta file, alternating between seq an title lines *)
  let rec readOneMoreLine seqdate seq counting record =
    match input_line fasta_file with
    |exception End_of_file -> 
        begin
          close_in fasta_file;
          let counting, record = update seqdate seq counting record in
          counting, record
        end;
    |s ->
        (* remove annoying special characters like \n *)
        let s = String.trim s in

        (* if we are reading a title line, we extract the date and analyze the previous sequence *)
        if String.get s 0 = '>' then 
          let exploded_line = (String.split_on_char '|' s) in
          let new_seqdate = List.hd (List.rev exploded_line) in
          if seq = "" then 
            readOneMoreLine new_seqdate seq counting record
          else
            let counting, record = update seqdate seq counting record in
            readOneMoreLine new_seqdate "" counting record

        (* if we are dealing with a line for the sequence, we append it to the previously read seq *)
        else
          let new_seq = seq ^ s in
          readOneMoreLine seqdate new_seq counting record in

  readOneMoreLine "" "" (0,0,0,0,0,0,0) (refseq::[], [], [])



let _ =
  (*
  let region_short = "IT" in
  let region_short = "FR" in
  let region_short = "DE" in
  *)
  let region_short = "CH" in

  (* location of the file for reading metadata *)
  let base_data_folder = "../data/" in
  let metadata_filename = base_data_folder ^ region_short ^ "/gisaid_hcov-19_2021_06_14_15.tsv" in
  let metadata_info = read_in_short_csv metadata_filename in

  (* location of the file for reading raw sequences *)
  let fasta_filename = base_data_folder ^ region_short ^ "/gisaid_hcov-19_2021_06_14_15.fasta" in
  let fasta_file = open_in fasta_filename in

  (* records the allele partition for data up to this date *)
  let date_max = date_of_string "2020-06-01" in

  (* we first get the list of alleles and the list of processed sequences *)
  let counting, record = collect_allele_partition fasta_file metadata_info date_max in
  let list_of_refseq, list_of_alleles, processed_dates = record in
  let incorrect_date, too_recent, prop_n, gap, start_not_found, too_small, ok = counting in
  Printf.printf "Result of the screening of the fasta file for region %s:
    We reject:
      - %i incorrect dates,
      - %i dates too recent,
      - %i seq with too high prop of n's,
      - %i seq where the start was not found,
      - %i seq being too small,
      - %i seq with a gap or something weird.
    We keep:
      - %i seqs,
      - among which %i where distinct enough to be called refseqs,
      - classified in %i alleles. \n" 
    region_short incorrect_date too_recent prop_n start_not_found too_small gap ok (List.length list_of_refseq) (List.length list_of_alleles);

  (* we order the list of processed sequences by date backward in time *)
  let get_backward_date datestring =
    let date = date_of_string datestring in
    day_number_between_two_dates date date_max in
  let processed_backward = List.map get_backward_date processed_dates in
  let processed = List.sort compare processed_backward in

  (* and we order as well the sampling dates within each allele:
    * to do that we explore them all and transform
    * the date into the date backward from the max date,
    * before ordering. *)
  let rec get_dates_within_alleles all_samp_times = function
    |[] -> all_samp_times
    |(all, lseqdates)::tl ->
        let new_backward_dates = List.map get_backward_date lseqdates in
        let new_samp_times = (List.sort compare new_backward_dates)::all_samp_times in
        get_dates_within_alleles new_samp_times tl in
  let all_samp_times = get_dates_within_alleles [] list_of_alleles in

  (* before writing out these two int lists *)
  let processed_filename = "../data-transformed/dataset/" ^ region_short ^ "_samp_history.csv" in
  let all_samp_times_filename = "../data-transformed/dataset/" ^ region_short ^ "_alleles.csv" in
  let processed_file = open_out processed_filename in
  let all_samp_times_file = open_out all_samp_times_filename in
  begin
    output_string processed_file (string_of_list string_of_int "," processed);
    close_out processed_file;
    output_string all_samp_times_file (string_of_list (string_of_list string_of_int ",") "\n" all_samp_times);
    (*output_list (string_of_list string_of_int ";") "\n" false all_samp_times_file all_samp_times;*)
    close_out all_samp_times_file
  end;;
  
