open Unix
open Coalescent
open Out

let test_ess list_possible_Svalues nsteps ndata_same_size nchains =

  let tchange_N = 0.::20.::40.::60.::80.::[] in
  let build_listN e = (e, 1000.) in
  let listN = List.map build_listN tchange_N in

  let tchange_S = 0.::10.::30.::50.::70.::90.::[] in

  let repo = "../simulation-outputs/test_ess/" in
  let _ = Unix.chdir repo in
  let mu = 0.005 in
  let g = 1. in
  let hyperprior_N = (0., 1000., 0.001) in
  let hyperprior_mu = (1., 200.) in
  let hyperprior_g = (10., 10.) in
  let tor = infinity in

  let rec aux = function 
    |[] -> ()
    |hd::tl -> 
        (* build the list of S values, with a zero as the final value *)
        let build_listS i e =
          if i = (List.length tchange_S) - 1 then (e, 0.)
          else (e, hd) in
        let listS = List.mapi build_listS tchange_S in

        let folder_S = ("S"^(string_of_float hd)) in
        let _ =
          begin
            Unix.mkdir folder_S 0o777 ;
            Unix.chdir folder_S
          end in

        (* the hyperprior that we consider is on the same scale as the true value *)
        let hyperprior_S = (1., 1./.hd) in
        let hyperpriors = (hyperprior_N, hyperprior_S, hyperprior_mu, hyperprior_g, tor) in

        let rec repeat_for_many_datasets = function
          |0 -> 
              begin
                Unix.chdir "../";
                aux tl
              end
          |n ->
              let folder_dataset = ("dataset"^(string_of_int n)) in
              let _ =
                begin
                  Unix.mkdir folder_dataset 0o777;
                  Unix.chdir folder_dataset
                end in

              (* simulate the dataset according to the model, so here there is a bit of stochasticity in the dataset *)
              let samp_history = sim_sampling_events listS listN in
              let alleles, coal_history = sim_coal mu g listN samp_history in
              let array_individuals0, array_alleles0 = get_info_arrays samp_history alleles in

              let rec repeat_for_many_chains = function
                |0 -> 
                    begin
                      Unix.chdir "../";
                      repeat_for_many_datasets (n-1)
                    end
                |p ->
                    let folder_chain = ("chain"^(string_of_int p)) in
                    let _ = Unix.mkdir folder_chain 0o777 in

                    (* run the MCMC and sample the posterior distribution *)
                    let array_individuals = Array.copy array_individuals0 in
                    let array_alleles = Array.copy array_alleles0 in
                    (* initialize the list of all_events for the MCMC *)
                    let init_state = gibbs_sampling_init array_individuals array_alleles hyperpriors tchange_N tchange_S in

                    let _ =
                      gibbs_sampling init_state array_individuals array_alleles hyperpriors (0, nsteps, 1) folder_chain false true in
                    repeat_for_many_chains (p-1) in

              repeat_for_many_chains nchains in

        repeat_for_many_datasets ndata_same_size in

  aux list_possible_Svalues

let _ =
  let _ = Random.init 10 in
  let list_possible_Svalues = 0.0001::0.0002::0.0004::0.0008::0.0016::0.0032::0.0064::0.0128::0.0256::0.0512::[] in
  let nsteps = 10000 in
  let ndata_same_size = 1 in
  let nchains = 1 in
  test_ess list_possible_Svalues nsteps ndata_same_size nchains
