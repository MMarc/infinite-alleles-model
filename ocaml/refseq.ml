open Nt
open Aa
open In
open Allele

let refname = "Wuhan/WIV04/2019"

let refseq_uncut =
  let infile = "../data/reference/EPI_ISL_402124.fasta" in
  read_seq_multiline infile


(* not sure the following positions still hold true now that I changed
 * the reference sequence to the one chosen by gisaid *)
let start_orf1a = 265
let end_orf1a = 13470
let start_orf1b = 13467
let end_orf1b = 21554

let start_s = 21562
let end_s = 25383

let start_orf3a = 25392
let end_orf3a = 26219

let start_e = 26244
let end_e = 26471

let start_m = 26522
let end_m = 27190

let start_orf6 = 27201
let end_orf6 = 27386

let start_orf7a = 27393
let end_orf7a = 27758

let start_orf8 = 27893
let end_orf8 = 28258

let start_n = 28273
let end_n = 29532

let start_orf10 = 29557
let end_orf10 = 29673

type gene = G_noncoding | G_orf1a | G_orf1b | G_orf1ab | G_s | G_orf3a | G_e | G_m | G_orf6 | G_orf7a | G_orf8 | G_n | G_orf10

(** [position_in_prot i] is a tuple [(gene, pos)] where [gene] is the gene that nucleotide belongs to, and [pos] is an int between 0 and 2 giving the index of the nucleotide within its codon. *)
let position_in_prot i =
  if i < start_orf1a then
    (G_noncoding, i mod 3)
  else if i < end_orf1a then
    (G_orf1a, (i-start_orf1a) mod 3)
  else if i < end_orf1b then
    (G_orf1b, (i-start_orf1b) mod 3)

  else if i < start_s then
    (G_noncoding, 1)
  else if i < end_s then
    (G_s, (i-start_s) mod 3)

  else if i < start_orf3a then
    (G_noncoding, 1)
  else if i < end_orf3a then
    (G_orf3a, (i-start_orf3a) mod 3)

  else if i < start_e then
    (G_noncoding, 1)
  else if i < end_e then
    (G_e, (i-start_e) mod 3)

  else if i < start_m then
    (G_noncoding, 1)
  else if i < end_m then
    (G_m, (i-start_m) mod 3)

  else if i < start_orf6 then
    (G_noncoding, 1)
  else if i < end_orf6 then
    (G_orf6, (i-start_orf6) mod 3)

  else if i < start_orf7a then
    (G_noncoding, 1)
  else if i < end_orf7a then
    (G_orf7a, (i-start_orf7a) mod 3)

  else if i < start_orf8 then
    (G_noncoding, 1)
  else if i < end_orf8 then
    (G_orf8, (i-start_orf8) mod 3)

  else if i < start_n then
    (G_noncoding, 1)
  else if i < end_n then
    (G_n, (i-start_n) mod 3)

  else if i < start_orf10 then
    (G_noncoding, 1)
  else if i < end_orf10 then
    (G_orf10, (i-start_orf10) mod 3)

  else (G_noncoding, 1)

(** [gene_seq gene seq] is the subsequence string of [seq] corresponding to gene [gene]. *)
let gene_seq gene seq =
  let startpos, endpos =
    if gene = G_orf1ab then 
      start_orf1a, end_orf1b
    else if gene = G_orf1a then
      start_orf1a, end_orf1a
    else if gene = G_orf1b then
      start_orf1b, end_orf1b
    else if gene = G_s then
      start_s, end_s
    else if gene = G_orf3a then
      start_orf3a, end_orf3a
    else if gene = G_e then
      start_e, end_e
    else if gene = G_m then
      start_m, end_m
    else if gene = G_orf6 then
      start_orf6, end_orf6
    else if gene = G_orf7a then
      start_orf7a, end_orf7a
    else if gene = G_orf8 then
      start_orf8, end_orf8
    else if gene = G_n then
      start_n, end_n
    else if gene = G_orf10 then
      start_orf10, end_orf10
    else
      0,0
  in String.sub seq startpos (endpos-startpos)

(** is the traduction of the string of nucleotides into a string of amino-acids. *)
let aastring_of_ntstring seq =
  let nb_aa = (String.length seq) / 3
  in let get_aa i = 
    let nt1 = nt_of_char (seq.[i*3])
    in let nt2 = nt_of_char (seq.[i*3+1])
    in let nt3 = nt_of_char (seq.[i*3+2])
    in char_of_aa (aa_of_codon (nt1, nt2, nt3))
  in String.init nb_aa get_aa

(** is the string of amino-acids of a given [gene]. *)
let rec aastring_of_gene gene =
  match gene with
  |G_orf1ab -> 
      let string1 = aastring_of_gene G_orf1a
      in let string2 = aastring_of_gene G_orf1b
      in String.concat "" (string1::string2::[])
  |_ -> aastring_of_ntstring (gene_seq gene refseq_uncut)

(** the nucleotide of the reference sequence at position [i]. *)
let ntref i = nt_of_char (refseq_uncut.[i])

(** the codon in the reference sequence at position [pos_aa]. Note that a codon is simply a triplet of nucleotides here. *)
let codonref pos_aa =
  let uref = ntref (pos_aa)
  in let vref = ntref (pos_aa+1)
  in let wref = ntref (pos_aa+2)
  in (uref, vref, wref)

(** [aaallele_of_ntallele ntall] is the traduction of the nucleotide allele [ntall] - 
 * which is a list of tuples [Ntsnp(pos, nt)] - into an amino-acid allele 
 * - i.e. a lit of tuples [Aasnp (pos, aa)]. i
 * The new position in the aa allele corresponds to the index of the first nucleotide of the codon.
 * Note that we completely forget about INS and DEL here. *)
let rec aaallele_of_ntallele ntall =
  (* we define an accessory function "acc" which is tail-recursive *)
  let rec acc ntall aaall =
    match ntall with
    |[] -> List.rev aaall

    |Ntsnp(x1,nt1)::[] ->
        let (gene1, pos1) = position_in_prot x1
        in if gene1 = G_noncoding then
          acc [] aaall
        (* starting here, nt1 must be taken into account*)
        else
          let pos_aa = x1 - pos1
          in let (uref, vref, wref) = codonref pos_aa
          in let u = if pos1 = 0 then nt1 else uref
          in let v = if pos1 = 1 then nt1 else vref
          in let w = if pos1 = 2 then nt1 else wref
          in let newaa = aa_of_codon (u,v,w)
          in let refaa = aa_of_codon (uref, vref, wref)
          in let new_aaall = if newaa = refaa then aaall else Aasnp(pos_aa, newaa)::aaall
          in acc [] new_aaall
   
    |Ntsnp(x1,nt1)::Ntsnp(x2,nt2)::[] ->
        let (gene1, pos1) = position_in_prot x1
        in if gene1 = G_noncoding then
          acc (Ntsnp(x2,nt2)::[]) aaall
        (* starting here, nt1 must be taken into account*)
        else
          let pos_aa = x1 - pos1
          in let (gene2, pos2) = position_in_prot x2
          in if x2 > x1 + 2 || pos2 < pos1 then
            let new_aaall = 
              match acc (Ntsnp(x1,nt1)::[]) [] with
              |Aasnp(x, aa)::tl -> Aasnp(x,aa)::aaall
              |_ -> aaall
            in acc (Ntsnp(x2,nt2)::[]) new_aaall
          (* starting here, nt2 must be taken into account as part of the same codon *)
          else
            let (uref, vref, wref) = codonref pos_aa
            in let u = if pos1 = 0 then nt1 else uref
            in let v = if pos1 = 1 then nt1 else if pos2 = 1 then nt2 else vref
            in let w = if pos2 = 2 then nt2 else wref
            in let newaa = aa_of_codon (u,v,w)
            in let refaa = aa_of_codon (uref, vref, wref)
            in let new_aaall = if newaa = refaa then aaall else Aasnp(pos_aa, newaa)::aaall
            in acc [] new_aaall

    |Ntsnp(x1,nt1)::Ntsnp(x2,nt2)::Ntsnp(x3,nt3)::tl ->
        let (gene1, pos1) = position_in_prot x1
        in if gene1 = G_noncoding then
          acc (Ntsnp(x2,nt2)::Ntsnp(x3,nt3)::tl) aaall
        (* starting here, nt1 must be taken into account*)
        else
          let pos_aa = x1 - pos1
          in let (gene2, pos2) = position_in_prot x2
          in if x2 > x1 + 2 || pos2 < pos1 then
            let new_aaall = 
              match acc (Ntsnp(x1,nt1)::[]) [] with
              |Aasnp(x, aa)::tl -> Aasnp(x,aa)::aaall
              |_ -> aaall
            in acc (Ntsnp(x2,nt2)::Ntsnp(x3,nt3)::tl) new_aaall
          (* starting here, nt2 must be taken into account as part of the same codon *)
          else
            let (gene3, pos3) = position_in_prot x3
            in if x3 > x1 + 2 || pos3 < pos2 then
              let new_aaall = 
                match acc (Ntsnp(x1,nt1)::Ntsnp(x2,nt2)::[]) [] with
                |Aasnp(x,aa)::tl -> Aasnp(x,aa)::aaall
                |_ -> aaall
              in acc (Ntsnp(x3,nt3)::tl) new_aaall
            (* starting here, nt3 must be taken into account as part of the same codon *)
            else
              let newaa = aa_of_codon (nt1, nt2, nt3)
              in let refcodon = codonref pos_aa
              in let refaa = aa_of_codon refcodon
              in let new_aaall = if newaa = refaa then aaall else Aasnp(pos_aa, newaa)::aaall
              in acc tl new_aaall
             
    |_::tl ->
        acc tl aaall

  in acc ntall []


let rec distance_p_along_seq start step fixed_all all res curr_dist =
  match start with
  |30000 -> res
  |i -> 
      match all with 
      |[] -> 
          distance_p_along_seq (start+step) step fixed_all fixed_all (curr_dist::res) 0
      |(i,nt)::tl when i < start -> 
          distance_p_along_seq start step fixed_all tl res curr_dist
      |(i,nt)::tl when i > (start+step) ->
          distance_p_along_seq start step fixed_all tl res curr_dist
      |(i,nt)::tl ->
        let comp = is_mutation (nt_of_char refseq_uncut.[i]) nt 
        in let score = if comp then 1 else 0
        in let newdist = curr_dist + score
        in distance_p_along_seq start step fixed_all tl res newdist


let rec distance_nt all comparison_nt dist =
  match all with
  |[] -> dist
  |(i,nt)::tl ->
      let comp = comparison_nt (nt_of_char refseq_uncut.[i]) nt 
      in let score = if comp then 1 else 0
      in let newdist = dist + score
      in distance_nt tl comparison_nt newdist

(** is the number of nucleotides different in a given nucleotide allele [all] as compared to the reference sequence. Note that it is equivalent to the length of the allele. *)
let distance_p all =
  distance_nt all is_mutation 0

(** is the number of transitions between the reference sequence and the nucleotide allele [all]. *)
let distance_transition all =
  distance_nt all is_transition 0

(** is the number of transversions between the reference sequence and the nucleotide allele [all]. *)
let distance_transversion all =
  distance_nt all is_transversion 0

let rec distance_in_gene gene ntall d =
  match ntall with
  |[] -> d
  |(x,nt)::tl ->
      let (gene_touched, pos) = position_in_prot x
      in let newdist =
        if gene_touched = gene then (d+1) else d
      in distance_in_gene gene tl newdist

(** is the number of mutations in the nucleotide allele [all] falling in the non-coding part of the reference sequence. *)
let distance_noncoding all =
  distance_in_gene G_noncoding all 0

(** is the number of mutations at the amino-acid level between a nucleotide allele and the reference sequence. *)
let distance_nonsynonymous ntall =
  let aaall = aaallele_of_ntallele ntall
  in List.length aaall


(*

let ref_orf1ab = "MESLVPGFNEKTHVQLSLPVLQVRDVLVRGFGDSVEEVLSEARQHLKDGTCGLVEVEKGVLPQLEQPYVFIKRSDARTAPHGHVMVELVAELEGIQYGRSGETLGVLVPHVGEIPVAYRKVLLRKNGNKGAGGHSYGADLKSFDLGDELGTDPYEDFQENWNTKHSSGVTRELMRELNGGAYTRYVDNNFCGPDGYPLECIKDLLARAGKASCTLSEQLDFIDTKRGVYCCREHEHEIAWYTERSEKSYELQTPFEIKLAKKFDTFNGECPNFVFPLNSIIKTIQPRVEKKKLDGFMGRIRSVYPVASPNECNQMCLSTLMKCDHCGETSWQTGDFVKATCEFCGTENLTKEGATTCGYLPQNAVVKIYCPACHNSEVGPEHSLAEYHNESGLKTILRKGGRTIAFGGCVFSYVGCHNKCAYWVPRASANIGCNHTGVVGEGSEGLNDNLLEILQKEKVNINIVGDFKLNEEIAIILASFSASTSAFVETVKGLDYKAFKQIVESCGNFKVTKGKAKKGAWNIGEQKSILSPLYAFASEAARVVRSIFSRTLETAQNSVRVLQKAAITILDGISQYSLRLIDAMMFTSDLATNNLVVMAYITGGVVQLTSQWLTNIFGTVYEKLKPVLDWLEEKFKEGVEFLRDGWEIVKFISTCACEIVGGQIVTCAKEIKESVQTFFKLVNKFLALCADSIIIGGAKLKALNLGETFVTHSKGLYRKCVKSREETGLLMPLKAPKEIIFLEGETLPTEVLTEEVVLKTGDLQPLEQPTSEAVEAPLVGTPVCINGLMLLEIKDTEKYCALAPNMMVTNNTFTLKGGAPTKVTFGDDTVIEVQGYKSVNITFELDERIDKVLNEKCSAYTVELGTEVNEFACVVADAVIKTLQPVSELLTPLGIDLDEWSMATYYLFDESGEFKLASHMYCSFYPPDEDEEEGDCEEEEFEPSTQYEYGTEDDYQGKPLEFGATSAALQPEEEQEEDWLDDDSQQTVGQQDGSEDNQTTTIQTIVEVQPQLEMELTPVVQTIEVNSFSGYLKLTDNVYIKNADIVEEAKKVKPTVVVNAANVYLKHGGGVAGALNKATNNAMQVESDDYIATNGPLKVGGSCVLSGHNLAKHCLHVVGPNVNKGEDIQLLKSAYENFNQHEVLLAPLLSAGIFGADPIHSLRVCVDTVRTNVYLAVFDKNLYDKLVSSFLEMKSEKQVEQKIAEIPKEEVKPFITESKPSVEQRKQDDKKIKACVEEVTTTLEETKFLTENLLLYIDINGNLHPDSATLVSDIDITFLKKDAPYIVGDVVQEGVLTAVVIPTKKAGGTTEMLAKALRKVPTDNYITTYPGQGLNGYTVEEAKTVLKKCKSAFYILPSIISNEKQEILGTVSWNLREMLAHAEETRKLMPVCVETKAIVSTIQRKYKGIKIQEGVVDYGARFYFYTSKTTVASLINTLNDLNETLVTMPLGYVTHGLNLEEAARYMRSLKVPATVSVSSPDAVTAYNGYLTSSSKTPEEHFIETISLAGSYKDWSYSGQSTQLGIEFLKRGDKSVYYTSNPTTFHLDGEVITFDNLKTLLSLREVRTIKVFTTVDNINLHTQVVDMSMTYGQQFGPTYLDGADVTKIKPHNSHEGKTFYVLPNDDTLRVEAFEYYHTTDPSFLGRYMSALNHTKKWKYPQVNGLTSIKWADNNCYLATALLTLQQIELKFNPPALQDAYYRARAGEAANFCALILAYCNKTVGELGDVRETMSYLFQHANLDSCKRVLNVVCKTCGQQQTTLKGVEAVMYMGTLSYEQFKKGVQIPCTCGKQATKYLVQQESPFVMMSAPPAQYELKHGTFTCASEYTGNYQCGHYKHITSKETLYCIDGALLTKSSEYKGPITDVFYKENSYTTTIKPVTYKLDGVVCTEIDPKLDNYYKKDNSYFTEQPIDLVPNQPYPNASFDNFKFVCDNIKFADDLNQLTGYKKPASRELKVTFFPDLNGDVVAIDYKHYTPSFKKGAKLLHKPIVWHVNNATNKATYKPNTWCIRCLWSTKPVETSNSFDVLKSEDAQGMDNLACEDLKPVSEEVVENPTIQKDVLECNVKTTEVVGDIILKPANNSLKITEEVGHTDLMAAYVDNSSLTIKKPNELSRVLGLKTLATHGLAAVNSVPWDTIANYAKPFLNKVVSTTTNIVTRCLNRVCTNYMPYFFTLLLQLCTFTRSTNSRIKASMPTTIAKNTVKSVGKFCLEASFNYLKSPNFSKLINIIIWFLLLSVCLGSLIYSTAALGVLMSNLGMPSYCTGYREGYLNSTNVTIATYCTGSIPCSVCLSGLDSLDTYPSLETIQITISSFKWDLTAFGLVAEWFLAYILFTRFFYVLGLAAIMQLFFSYFAVHFISNSWLMWLIINLVQMAPISAMVRMYIFFASFYYVWKSYVHVVDGCNSSTCMMCYKRNRATRVECTTIVNGVRRSFYVYANGGKGFCKLHNWNCVNCDTFCAGSTFISDEVARDLSLQFKRPINPTDQSSYIVDSVTVKNGSIHLYFDKAGQKTYERHSLSHFVNLDNLRANNTKGSLPINVIVFDGKSKCEESSAKSASVYYSQLMCQPILLLDQALVSDVGDSAEVAVKMFDAYVNTFSSTFNVPMEKLKTLVATAEAELAKNVSLDNVLSTFISAARQGFVDSDVETKDVVECLKLSHQSDIEVTGDSCNNYMLTYNKVENMTPRDLGACIDCSARHINAQVAKSHNIALIWNVKDFMSLSEQLRKQIRSAAKKNNLPFKLTCATTRQVVNVVTTKIALKGGKIVNNWLKQLIKVTLVFLFVAAIFYLITPVHVMSKHTDFSSEIIGYKAIDGGVTRDIASTDTCFANKHADFDTWFSQRGGSYTNDKACPLIAAVITREVGFVVPGLPGTILRTTNGDFLHFLPRVFSAVGNICYTPSKLIEYTDFATSACVLAAECTIFKDASGKPVPYCYDTNVLEGSVAYESLRPDTRYVLMDGSIIQFPNTYLEGSVRVVTTFDSEYCRHGTCERSEAGVCVSTSGRWVLNNDYYRSLPGVFCGVDAVNLLTNMFTPLIQPIGALDISASIVAGGIVAIVVTCLAYYFMRFRRAFGEYSHVVAFNTLLFLMSFTVLCLTPVYSFLPGVYSVIYLYLTFYLTNDVSFLAHIQWMVMFTPLVPFWITIAYIICISTKHFYWFFSNYLKRRVVFNGVSFSTFEEAALCTFLLNKEMYLKLRSDVLLPLTQYNRYLALYNKYKYFSGAMDTTSYREAACCHLAKALNDFSNSGSDVLYQPPQTSITSAVLQSGFRKMAFPSGKVEGCMVQVTCGTTTLNGLWLDDVVYCPRHVICTSEDMLNPNYEDLLIRKSNHNFLVQAGNVQLRVIGHSMQNCVLKLKVDTANPKTPKYKFVRIQPGQTFSVLACYNGSPSGVYQCAMRPNFTIKGSFLNGSCGSVGFNIDYDCVSFCYMHHMELPTGVHAGTDLEGNFYGPFVDRQTAQAAGTDTTITVNVLAWLYAAVINGDRWFLNRFTTTLNDFNLVAMKYNYEPLTQDHVDILGPLSAQTGIAVLDMCASLKELLQNGMNGRTILGSALLEDEFTPFDVVRQCSGVTFQSAVKRTIKGTHHWLLLTILTSLLVLVQSTQWSLFFFLYENAFLPFAMGIIAMSAFAMMFVKHKHAFLCLFLLPSLATVAYFNMVYMPASWVMRIMTWLDMVDTSLSGFKLKDCVMYASAVVLLILMTARTVYDDGARRVWTLMNVLTLVYKVYYGNALDQAISMWALIISVTSNYSGVVTTVMFLARGIVFMCVEYCPIFFITGNTLQCIMLVYCFLGYFCTCYFGLFCLLNRYFRLTLGVYDYLVSTQEFRYMNSQGLLPPKNSIDAFKLNIKLLGVGGKPCIKVATVQSKMSDVKCTSVVLLSVLQQLRVESSSKLWAQCVQLHNDILLAKDTTEAFEKMVSLLSVLLSMQGAVDINKLCEEMLDNRATLQAIASEFSSLPSYAAFATAQEAYEQAVANGDSEVVLKKLKKSLNVAKSEFDRDAAMQRKLEKMADQAMTQMYKQARSEDKRAKVTSAMQTMLFTMLRKLDNDALNNIINNARDGCVPLNIIPLTTAAKLMVVIPDYNTYKNTCDGTTFTYASALWEIQQVVDADSKIVQLSEISMDNSPNLAWPLIVTALRANSAVKLQNNELSPVALRQMSCAAGTTQTACTDDNALAYYNTTKGGRFVLALLSDLQDLKWARFPKSDGTGTIYTELEPPCRFVTDTPKGPKVKYLYFIKGLNNLNRGMVLGSLAATVRLQAGNATEVPANSTVLSFCAFAVDAAKAYKDYLASGGQPITNCVKMLCTHTGTGQAITVTPEANMDQESFGGASCCLYCRCHIDHPNPKGFCDLKGKYVQIPTTCANDPVGFTLKNTVCTVCGMWKGYGCSCDQLREPMLQSADAQSFLNRVCGVSAARLTPCGTGTSTDVVYRAFDIYNDKVAGFAKFLKTNCCRFQEKDEDDNLIDSYFVVKRHTFSNYQHEETIYNLLKDCPAVAKHDFFKFRIDGDMVPHISRQRLTKYTMADLVYALRHFDEGNCDTLKEILVTYNCCDDDYFNKKDWYDFVENPDILRVYANLGERVRQALLKTVQFCDAMRNAGIVGVLTLDNQDLNGNWYDFGDFIQTTPGSGVPVVDSYYSLLMPILTLTRALTAESHVDTDLTKPYIKWDLLKYDFTEERLKLFDRYFKYWDQTYHPNCVNCLDDRCILHCANFNVLFSTVFPPTSFGPLVRKIFVDGVPFVVSTGYHFRELGVVHNQDVNLHSSRLSFKELLVYAADPAMHAASGNLLLDKRTTCFSVAALTNNVAFQTVKPGNFNKDFYDFAVSKGFFKEGSSVELKHFFFAQDGNAAISDYDYYRYNLPTMCDIRQLLFVVEVVDKYFDCYDGGCINANQVIVNNLDKSAGFPFNKWGKARLYYDSMSYEDQDALFAYTKRNVIPTITQMNLKYAISAKNRARTVAGVSICSTMTNRQFHQKLLKSIAATRGATVVIGTSKFYGGWHNMLKTVYSDVENPHLMGWDYPKCDRAMPNMLRIMASLVLARKHTTCCSLSHRFYRLANECAQVLSEMVMCGGSLYVKPGGTSSGDATTAYANSVFNICQAVTANVNALLSTDGNKIADKYVRNLQHRLYECLYRNRDVDTDFVNEFYAYLRKHFSMMILSDDAVVCFNSTYASQGLVASIKNFKSVLYYQNNVFMSEAKCWTETDLTKGPHEFCSQHTMLVKQGDDYVYLPYPDPSRILGAGCFVDDIVKTDGTLMIERFVSLAIDAYPLTKHPNQEYADVFHLYLQYIRKLHDELTGHMLDMYSVMLTNDNTSRYWEPEFYEAMYTPHTVLQAVGACVLCNSQTSLRCGACIRRPFLCCKCCYDHVISTSHKLVLSVNPYVCNAPGCDVTDVTQLYLGGMSYYCKSHKPPISFPLCANGQVFGLYKNTCVGSDNVTDFNAIATCDWTNAGDYILANTCTERLKLFAAETLKATEETFKLSYGIATVREVLSDRELHLSWEVGKPRPPLNRNYVFTGYRVTKNSKVQIGEYTFEKGDYGDAVVYRGTTTYKLNVGDYFVLTSHTVMPLSAPTLVPQEHYVRITGLYPTLNISDEFSSNVANYQKVGMQKYSTLQGPPGTGKSHFAIGLALYYPSARIVYTACSHAAVDALCEKALKYLPIDKCSRIIPARARVECFDKFKVNSTLEQYVFCTVNALPETTADIVVFDEISMATNYDLSVVNARLRAKHYVYIGDPAQLPAPRTLLTKGTLEPEYFNSVCRLMKTIGPDMFLGTCRRCPAEIVDTVSALVYDNKLKAHKDKSAQCFKMFYKGVITHDVSSAINRPQIGVVREFLTRNPAWRKAVFISPYNSQNAVASKILGLPTQTVDSSQGSEYDYVIFTQTTETAHSCNVNRFNVAITRAKVGILCIMSDRDLYDKLQFTSLEIPRRNVATLQAENVTGLFKDCSKVITGLHPTQAPTHLSVDTKFKTEGLCVDIPGIPKDMTYRRLISMMGFKMNYQVNGYPNMFITREEAIRHVRAWIGFDVEGCHATREAVGTNLPLQLGFSTGVNLVAVPTGYVDTPNNTDFSRVSAKPPPGDQFKHLIPLMYKGLPWNVVRIKIVQMLSDTLKNLSDRVVFVLWAHGFELTSMKYFVKIGPERTCCLCDRRATCFSTASDTYACWHHSIGFDYVYNPFMIDVQQWGFTGNLQSNHDLYCQVHGNAHVASCDAIMTRCLAVHECFVKRVDWTIEYPIIGDELKINAACRKVQHMVVKAALLADKFPVLHDIGNPKAIKCVPQADVEWKFYDAQPCSDKAYKIEELFYSYATHSDKFTDGVCLFWNCNVDRYPANSIVCRFDTRVLSNLNLPGCDGGSLYVNKHAFHTPAFDKSAFVNLKQLPFFYYSDSPCESHGKQVVSDIDYVPLKSATCITRCNLGGAVCRHHANEYRLYLDAYNMMISAGFSLWVYKQFDTYNLWNTFTRLQSLENVAFNVVNKGHFDGQQGEVPVSIINNTVYTKVDGVDVELFENKTTLPVNVAFELWAKRNIKPVPEVKILNNLGVDIAANTVIWDYKRDAPAHISTIGVCSMTDIAKKPTETICAPLTVFFDGRVDGQVDLFRNARNGVLITEGSVKGLQPSVGPKQASLNGVTLIGEAVKTQFNYYKKVDGVVQQLPETYFTQSRNLQEFKPRSQMEIDFLELAMDEFIERYKLEGYAFEHIVYGDFSHSQLGGLHLLIGLAKRFKESPFELEDFIPMDSTVKNYFITDAQTGSSKCVCSVIDLLLDDFVEIIKSQDLSVVSKVVKVTIDYTEISFMLWCKDGHVETFYPKLQSSQAWQPGVAMPNLYKMQRMLLEKCDLQNYGDSATLPKGIMMNVAKYTQLCQYLNTLTLAVPYNMRVIHFGAGSDKGVAPGTAVLRQWLPTGTLLVDSDLNDFVSDADSTLIGDCATVHTANKWDLIISDMYDPKTKNVTKENDSKEGFFTYICGFIQQKLALGGSVAIKITEHSWNADLYKLMGHFAWWTAFVTNVNASSSEAFLIGCNYLGKPREQIDGYVMHANYIFWRNTNPIQLSSYSLFDMSKFPLKLRGTAVMSLKEGQINDMILSLLSKGRLIIRENNRVVISSDVLVNN"

in let ref_orf10 = "MGYINVFAFPFTIYSLLLCRMNSRNYIAQVDVVNFNLT"
in let our_orf1ab = aastring_of_gene G_orf1ab
in let our_orf10 = aastring_of_gene G_orf10
(*in let infile = "../data/testforaa.fasta"
in let list_of_alleles = ref []
in let list_of_aaalleles = ref []
in let file = open_in infile
in let rec readOneMoreSeq i =
  match input_line file with
  |nameline ->
      let seqname = read_name_line nameline
      in let seq =
        match input_line file with
        |s -> s
        |exception End_of_file -> ""
      in let all =
        match compute_allele refseq seq with
        |exception AlleleWithGap -> Printf.printf "There is a gap\n"; []
        |all -> all
      in begin
          list_of_alleles := all::(!list_of_alleles);
          Printf.printf "Processing another seq\n";
          list_of_aaalleles := (aaallele_of_ntallele all)::(!list_of_aaalleles);
          readOneMoreSeq (i+1)
        end;
  |exception End_of_file -> 
      begin
        close_in file;
        output_allele_members2 ";" "ntalleles" (!list_of_alleles) (output_allele_string);
        output_allele_members2 ";" "aaalleles" (!list_of_aaalleles) (output_aaallele_string);
      end;
*)

in begin
  (*Printf.printf "Primer is found at position %d\n" start_pos;*)
  if ref_orf1ab = our_orf1ab then Printf.printf "Success !\n";
  Printf.printf "Length of the ref: %d\nLength of ours: %d\n" (String.length ref_orf1ab) (String.length our_orf1ab);
  Printf.printf "%s\n" our_orf1ab;
  if ref_orf10 = our_orf10 then Printf.printf "Success !\n";
  Printf.printf "Length of the ref: %d\nLength of ours: %d\n" (String.length ref_orf10) (String.length our_orf10);
  Printf.printf "%s\n" our_orf10;
  (*readOneMoreSeq 0;*)
end;;
*)
