open Out
open In
open Rvariable
open Allele
open List_addons

(**********************************
 * Helper functions 
 * ********************************)

(* on top of the list of N (or S), one finds the current N (or S).
* The next time of change is found in the second element,
* and the third element of the tuple is the tail of the list of N (or S). *)
let get_piecewise_info listN =
  match listN with
  |[] -> 0., infinity, []
  |(t1,n1)::[] -> n1, infinity, []
  |(t1,n1)::(t2,n2)::tl -> n1, t2, (t2,n2)::tl


(* on top of a "sampling history" list, one finds the next time of event,
 * and the number of the individual concerned by the change.
 * The function also returns the tail, i.e. list of future events. *)
let get_future_info = function
  |[] -> infinity, -1, []
  |(t,n)::tl -> t,n,tl


(* same for "coalescent histories". *)
let get_future_info_v2 = function
  |[] -> infinity, -1, []
  |(t,n,_)::tl -> t,n,tl

  
(** The sampling history, with numbered individuals,
 * corresponding to a float list list [alleles]. *)
let get_samp_history alleles =

  (* we remove the information about alleles by 
   * - ordering all sublists in increasing order
   * - merging them together while keeping the same order *)
  let rec aux = function
    |[] -> []
    |hd::[] -> hd
    |hd::snd::tl -> 
        let hd_ordered = List.sort compare hd in
        let snd_ordered = List.sort compare snd in
        aux ((List.merge compare hd_ordered snd_ordered)::tl) in
  let sampling_times = aux alleles in

  (* and then associate a unique number to each individual,
   * according to its rank in the ordered list of sampling times. *)
  let add_number i t = (t,i) in
  List.mapi add_number (List.rev sampling_times)


(** The float list list obtained by transforming
 * each int from an int list list by its corresponding
 * float in a float*int list [samp_history]. *)
let rm_individual_numbering allele_int samp_history =
  let get_birth_time i = 
    let is_i (_,j) = (i = j) in
    let (bi,_) = List.find is_i samp_history in
    bi in
  List.map (List.map get_birth_time) allele_int

  
(** Replace birth times in the [alleles] by numbers of 
* individuals having this birth time in [samp_history].
* Returns the resulting int list list compatible with
* the float list list given as [alleles] and the numbering
* of individuals given in [samp_history]. *)
let get_numbers_in_alleles samp_history alleles =

  (* ad-hoc way of numbering individuals within the alleles
  * by picking each time the first individual with same
  * birth time and removing it from the list.
  * subres is the current allele being explored,
  * res is the list of already explored alleles.*)
  let rec transform res subres samp_history = function
    |[] -> List.rev res
    |hd::tl ->
        match hd with
        |[] -> transform ((List.rev subres)::res) [] samp_history tl
        |hd2::tl2 ->
            (* look for the first individual having correct birth time *)
            let looking_for (t,_) =
              if t = hd2 then true
              else false in
            let i = find_first_index samp_history looking_for in
            (* remove it from the list and put its number 
              * in the allele subres being built *)
            let (_,n), new_samp_history = pop i samp_history in
            transform res (n::subres) new_samp_history (tl2::tl) in

  transform [] [] samp_history alleles


(** The interval drawn from the list of intervals [intervals], 
 * based on the specified [weight]. Each interval is a tuple
 * of 6 elements where the last one is the weight. *)
let find_back_interval weight intervals = 
  let rec aux cumsum = function
  (* because the weight is supposed to be less than the total cumulative sum of all elements, we are never supposed to find an empty list *)
  |[] -> 
      begin
        Printf.printf "The cumulative sum at then end is %f while the target was %f\n" cumsum weight;
        failwith "the weight is greater than the cumulative sum of weights"
      end
  |hd::tl ->
      let (_, _, _, _, _, w) = hd in
      let cumsum = cumsum +. w in
      if weight <= cumsum then hd
      else aux cumsum tl in
  aux 0. intervals


(**********************************
 * Coalescent process simulation
 * ********************************)

(** Coalescent history of a sample taken at present,
 * conditioned on satisfying the allele partition [narr],
 * with scaled mutation parameter [theta]. *)
let sim_kingman_cond_allele theta narr =
  
  let rec aux narr tlst etalst curr_t =
    (* the number of individuals remaining in the process *)
    let k = sum_arr narr in
    if k = 0 then (List.rev tlst), (List.rev etalst)
    else
        (* sample uniformly one group according to the number of individuals *)
        let chosen_lineage = sample_one_group narr in
        (* if there is a unique individual in the allele, it finds its mutation, otherwise, a coalescent event *)
        let new_etalst =  
          if narr.(chosen_lineage) = 1 then (true::etalst)
          else (false::etalst) in
        (* time at which the event happened *)
        let new_curr_t = 
          let rate = (float_of_int k) *. (theta +. float_of_int (k-1)) /. 2. in
          curr_t +. simExp rate in
        let new_tlst = new_curr_t::tlst in
        (* there is one less individual in the chosen allele, and we continue *)
        begin
          narr.(chosen_lineage) <- narr.(chosen_lineage) - 1;
          aux narr new_tlst new_etalst new_curr_t
        end in

  (* the recursive function modifies in place the array,
   * so to be safe here, we copy the array first and 
   * modify it only within the function above. *)
  let new_narr = Array.copy narr in
  aux new_narr [] [] 0.


(** The list of alleles (i.e. int list list where each int is the index of an individual), 
 * the coalescent history (i.e. an ordered list of death times (hi, ni, oi) 
 * where ni is the index of an individual dying and oi is the index of the individual recipient of the action),
 * and the eta_history (i.e. same order for (etai, ni), where etai is the boolean indicating a mutation),
 * all being the result of a coalescent process with mutation rate [mu],
 * population size through time [listN] and sampling history [samp_history]. *)
let sim_coal mu g listN samp_history =

  (* The list of active lineages, the list of fixed alleles,
   * and the id of the lineage going to fixation, obtained
   * by removing a uniformly sampled active lineage and inserting
   * it into the list of fixed lineages (a.k.a. alleles). *)
  let fix_one_lineage active_lineages alleles =
    let k = Random.int (List.length active_lineages) in
    let fixed_allele, new_active_lineages = pop k active_lineages in
    let lineage_id_dead = List.hd fixed_allele in
    let new_alleles = fixed_allele::alleles in
    new_active_lineages, new_alleles, lineage_id_dead, lineage_id_dead in

  (* The new list of active lineages obtained by uniformly
   * sampling two lineages, merging them and inserting the
   * result back into the old list. *)
  let coalesce_two_lineages active_lineages alleles =

    (* we sample and order the two indices *)
    let k, kprime = sample_two_without_replacement (List.length active_lineages) in
    let j, jprime = if k < kprime then k, kprime else kprime, k in

    (* remove the largest first, smallest then *)
    let chosen_lineage1, active_lineages_minus1 = pop jprime active_lineages in
    let chosen_lineage2, active_lineages_minus2 = pop j active_lineages_minus1 in

    (* the id of the lineages are the first (because smallest) value. *)
    let n1 = List.hd chosen_lineage1 in
    let n2 = List.hd chosen_lineage2 in
    (* and by convention, the lineage with largest id dies *)
    let lineage_id_dead, action = 
      if n1 < n2 then n2, n1 else n1, n2 in

    (* the resulting lineage is an ordered list of individuals' numbers. *)
    let merged_lineage = List.merge compare chosen_lineage1 chosen_lineage2 in
    let new_active_lineages = merged_lineage::active_lineages_minus2 in
    new_active_lineages, alleles, lineage_id_dead, action in
 
  (* simulate from bottom (present) to top (past) *)
  let rec next_event active_lineages future_lineages listN alleles coal_history t =
  
    match active_lineages, future_lineages with
    (* the simulation really ends only when there are no active lineages, no future lineages sampled later *)
    |[], [] -> alleles, List.rev coal_history
    |_ ->
        (* on top of the list of N, one finds the current one.
        * The next time of change is found in the second element. *)
        let currN, tchangeN, futureN = get_piecewise_info listN in

        (* the next time at which a sample will be added *)
        let t_add_samples, _, _ = get_future_info future_lineages in

        (* note here that k could be zero, because future lineages could be added later in the process. 
         * In this case, the next event happens after an infinite time, which means that there will be a sampling event before. *)
        let k = float_of_int (List.length active_lineages) in
        let mut_rate = mu *. k in
        let coal_rate = k *. (k -. 1.) /. (2. *. g *. currN) in
        let t_event = 
          let tot_rate = mut_rate +. coal_rate in
          if tot_rate = 0. then infinity
          else t +. simExp tot_rate in

        if t_event > t_add_samples || t_event > tchangeN then
          if t_add_samples < tchangeN then
            let (_, n) = List.hd future_lineages in
            let new_active_lineages = (n::[])::active_lineages in
            next_event new_active_lineages (List.tl future_lineages) listN alleles coal_history t_add_samples
          else
            next_event active_lineages future_lineages futureN alleles coal_history tchangeN
        else
          (* the boolean indicating if the event that happens at time t is a mutation *)
          let eta = Random.float 1. < mut_rate /. (mut_rate +. coal_rate) in
          (* the effect of a mutation or coalescence on active_lineages and alleles *)
          let new_active_lineages, new_alleles, lineage_id_dead, action =
            match eta with
              |true -> 
                  fix_one_lineage active_lineages alleles
              |false -> 
                  coalesce_two_lineages active_lineages alleles in
          let new_coal_history = (t_event, lineage_id_dead, action)::coal_history in
          next_event new_active_lineages future_lineages listN new_alleles new_coal_history t_event

  in next_event [] samp_history listN [] [] 0.


(** The list of alleles (i.e. int list list where each int is the index of an individual), 
 * the coalescent history (i.e. an ordered list of death times (hi, ni) where ni is the index of an individual),
 * and the eta_history (i.e. same order for (etai, ni), where etai is the boolean indicating a mutation),
 * all being the result of a coalescent process with mutation rate [mu],
 * population size through time [listN] and sampling history [samp_history]. *)
let sim_coal_horizontally mu g listN samp_history =

  (* insert a coalescent event into a list of coalescent history, ordered by death time *)
  let insert_coal (ti, ni, oi) coal_history =

    let rec aux lexplored = function
      (* if we have explored the full list, we must insert our element *)
      |[] -> 
          List.rev ((ti,ni,oi)::lexplored)

      (* we find the first time larger than our new time, so we insert ours *)
      |(tj, nj, oj)::tl  when tj > ti -> 
          let lexplored_new = (tj, nj, oj) :: (ti, ni, oi) :: lexplored in
          List.rev_append lexplored_new tl

      (* we find anything else *)
      |hd::tl -> 
          aux (hd::lexplored) tl in
   
    aux [] coal_history in

  (* get the list of intervals above time bi with a given sampling and coalescent history *) 
  let get_intervals_clean bi samp_history coal_history mu listN =

    let rec aux intervals tot_weight p_above samp_history coal_history listN tstart lineages =
      
      (* the recursion stops when we start an interval from infinity *)
      if tstart = infinity then intervals, tot_weight
      else

        let next_tdeath, indiv_death, future_coalH = get_future_info_v2 coal_history in
        let next_tbirth, indiv_birth, future_sampH = get_future_info samp_history in
        let currN, tchangeN, futureN = get_piecewise_info listN in
        let tend =
          if next_tdeath < next_tbirth && next_tdeath < tchangeN then next_tdeath
          else if next_tbirth < next_tdeath && next_tbirth < tchangeN then next_tbirth
          else tchangeN in

        (* update of the intervals with weights *)
        let new_intervals, new_tot_weight, new_p_above =
          if tstart = tend then
            intervals, tot_weight, p_above
          else
              let nb_tot = List.length lineages in
              let tot_rate = mu +. (float_of_int nb_tot) /. (g*.currN) in
              let p_t_not_in_interval = exp (-. tot_rate *. (tend -. tstart)) in
              let weight = p_above *. (1. -. p_t_not_in_interval) in
              let new_p_above = p_above *. p_t_not_in_interval in
              let interval = (tstart, tend, nb_tot, lineages, currN, weight) in
              (interval::intervals), tot_weight +. weight, new_p_above in

        (* update of the other parameters of the recursion. *)
        let new_lineages, new_samp_history, new_coal_history, new_listN =
          if tend = next_tdeath then
            begin
            (if indiv_death = -1 then lineages else remove_el indiv_death lineages),
            samp_history, 
            future_coalH, 
            listN
            end
          else if tend = next_tbirth then
            begin
            indiv_birth::lineages,
            future_sampH,
            coal_history,
            listN
            end
          else
            lineages, samp_history, coal_history, futureN in

        aux new_intervals new_tot_weight new_p_above new_samp_history new_coal_history new_listN tend new_lineages  in

    aux [] 0. 1. samp_history coal_history listN bi [] in

  (* update a list of alleles by putting ni in the allele where nj is found *)
  let rec update_alleles buffer ni nj = function
    |[] -> failwith "lineage is not found anywhere in the allele partition"
    |allele::tl ->
        let equal_nj x = (x = nj) in
        if List.exists equal_nj allele then List.rev_append buffer ((ni::allele)::tl)
        else update_alleles (allele::buffer) ni nj tl in

  (* the sampling history is here given as input with the oldest samples on top,
   * and we sample death events from left (oldest) to right (most recent sample).
   * Hence the samp_history_left contains the samples already added, with the most
   * recent on top. *)
  let rec aux samp_history_left alleles coal_history = function
    |[] -> alleles, coal_history
    |(bi, ni)::tl_samp_history ->

        (* compute the successive possible intervals with their weight and pick one *)
        let intervals, tot_weight = get_intervals_clean bi samp_history_left coal_history mu listN in
        let random = Random.float tot_weight in
        let (ts, te, nb_tot, lineages, currN, w) = find_back_interval random intervals in

        (* simulate the death time within the interval. *)
        let tot_rate = mu +. (float_of_int nb_tot) /. (g*.currN) in
        let tdeath = simExp_cond_interval tot_rate ts te in
     
        (* simulate the action happening *)
        let eta = Random.float 1. < mu /. tot_rate in
        let actioni =
          if eta then ni
          else List.nth lineages (Random.int (List.length lineages)) in
        let new_alleles = 
          if eta then (ni::[])::alleles
          else update_alleles [] ni actioni alleles in
        let new_coal_history = insert_coal (tdeath, ni, actioni) coal_history in

        aux ((bi,ni)::samp_history_left) new_alleles new_coal_history tl_samp_history in

  aux [] [] [] samp_history


(** The coalescent history and eta history obtained
 * by simulating the coalescent process with mutation
 * rate [mu], population sizes through time [listN],
 * conditioned on the allele partition [alleles], which
 * is an ordered float list list giving the sampling
 * times of individuals in an allele (i.e. individuals
 * are NOT numbered in this allele partition). *)
let sim_coal_cond_alleles use_horizontal_algo mu g listN alleles =

  (* auxiliary function to get ordered alleles (within alleles and between alleles as well) *)
  let get_unambiguous_allele_order all =
    let inside_ordered = List.map (List.sort compare) all in
    List.sort (compare_list compare) inside_ordered in

  (* the allele partition is ordered once for all, in case it is not given ordered. *)
  let alleles_ordered = get_unambiguous_allele_order alleles in
  (* we order birth events and number individuals accordingly. *)
  let samp_history = get_samp_history alleles in

  let rec aux () =
    (* we sample alleles with the individuals being numbered. *)
    let alleles_individuals_numbered, coal_history = 
      if use_horizontal_algo then
        sim_coal_horizontally mu g listN samp_history
      else 
        sim_coal mu g listN samp_history in
    (* forget about individual numbers by replacing with their birth time. *)
    let alleles_birthtime = rm_individual_numbering alleles_individuals_numbered samp_history in
    (* and finally order the resulting list for comparison purpose. *)
    let alleles_birthtime_ordered = get_unambiguous_allele_order alleles_birthtime in
    (*
    Printf.printf "Allele simulated: %s
    Allele without numbering: %s
    Allele ordered: %s
    And the comparison success: %s\n\n" 
    (string_of_list (string_of_list string_of_int ",") " | " alleles_individuals_numbered) 
    (string_of_list (string_of_list string_of_float ",") " | " alleles_birthtime)
    (string_of_list (string_of_list string_of_float ",") " | " alleles_birthtime_ordered) 
    (string_of_list (string_of_list string_of_float ",") " | " alleles_ordered);
    Printf.printf "Resulting coal history: %s\n" (string_of_coal_history false coal_history);
    *)

    if alleles_birthtime_ordered = alleles_ordered then coal_history
    else aux ()

  in aux ()


(**********************************
 * Full model simulation
 * ********************************)

(** The list of (ti,ni) where ti is the sampling time 
 * and ni is the individual's ID, 
 * obtained by sampling individuals as a PPP
 * with rate S_t N_t, where S and N are piecewise functions
 * given as [listS] and [listN].
 * The simulation is not performed if the last value of S or N,
 * is not S = 0 or N = 0. *)
let sim_sampling_events listS listN =

  (* check that the sampling rate becomes 0 in the deep past *)
  let (_, lastS) = List.nth listS ((List.length listS)-1) in
  let (_, lastN) = List.nth listN ((List.length listN)-1) in
  if lastN <> 0. && lastS <> 0. then failwith "Sampling times cannot be drawn if the sampling rate does not become 0 at some point."
  else

    let rec aux samp_times listS listN t =

      (* the simulation ends when reaching the last interval of change for S and N *)
      let currN, tchangeN, futureN = get_piecewise_info listN in
      let currS, tchangeS, futureS = get_piecewise_info listS in
      if tchangeS = infinity && tchangeN = infinity then samp_times
      else

        (* draw the number of sampling events. *)
        let samp_rate = currS *. currN in
        let deltat =
          if tchangeN < tchangeS then tchangeN -. t
          else tchangeS -. t in
        let n = simPoisson (samp_rate *. deltat) in
        if n = 0 then
          if tchangeN < tchangeS then aux samp_times listS futureN tchangeN
          else aux samp_times futureS listN tchangeS
        else
          let rec unordered_samp_times buffer = function
            |0 -> buffer
            |n -> 
                let time = t +. Random.float deltat in
                unordered_samp_times (time::buffer) (n-1) in
          let ordered_samp_times = List.sort compare (unordered_samp_times [] n) in
          let new_samp_times = List.rev_append ordered_samp_times samp_times in
          if tchangeN < tchangeS then
            aux new_samp_times listS futureN tchangeN
          else 
            aux new_samp_times futureS listN tchangeS in

    let decreasing_samp_times = aux [] listS listN 0. in
    let add_number i t = (t,i) in
    List.rev (List.mapi add_number decreasing_samp_times)

    
(* the pop size is GIG distributed at each time *)
let init_listN hyperprior_N tchange_N =
  let lambda, chi, psi = hyperprior_N in
  (*
  let sim_N_value i t = 
    if i = (List.length tchange_N) - 1 then (t, 1.)
    else (t, (simGIG lambda chi psi)) in
  List.mapi sim_N_value tchange_N
  *)
  let sim_N_value t = (t, (simGIG lambda chi psi)) in
  List.map sim_N_value tchange_N

(* the sampling intensity is Gamma distributed at each time
 * except at the last position where it is 0. *)
let init_listS hyperprior_S tchange_S =
  let alpha, beta = hyperprior_S in
  let sim_S_value i t = 
    if i = (List.length tchange_S) - 1 then (t, 0.)
    else (t, (simGamma alpha beta)) in
  List.mapi sim_S_value tchange_S


(* the mutation rate is Gamma distributed *)
let init_mu hyperprior_mu =
  let alpha, beta = hyperprior_mu in
  simGamma alpha beta


(* the generation time is Inv-Gamma distributed *)
let init_g hyperprior_g =
  let alpha, beta = hyperprior_g in
  simInvGamma alpha beta



(** Simulates a complete dataset according to the model, i.e.
 * - the mutation rate mu, Gamma distributed with param [hyperprior_mu],
 * - the list of N values, GIG distributed with param [hyperprior_N],
 * and changing points [tchange_N],
 * - the list of S values, Gamma distributed with param [hyperprior_S]
 * and changing points [tchange_S],
 * - the sampling history knowing listS and listN,
 * - the coalescent history knowing mu, listN and the sampling history.
 * Finally, it also returns the array of informations about individuals. *)
let sim_complete_dataset hyperpriors tchange_N tchange_S =
  (* first the parameters *)
  let hyperprior_N, hyperprior_S, hyperprior_mu, hyperprior_g, tor = hyperpriors in
  let listN = init_listN hyperprior_N tchange_N in
  let listS = init_listS hyperprior_S tchange_S in
  let mu = init_mu hyperprior_mu in
  let g = init_g hyperprior_g in

  (* the sampling and coalescent history then *)
  let samp_history = sim_sampling_events listS listN in
  let alleles, coal_history = sim_coal mu g listN samp_history in

  listN, listS, mu, g, samp_history, coal_history, alleles




(**********************************
 * Coalescent process inference
 * ********************************)

(** A punctual event can be the birth of a numbered individual,
 * or the death of a numbered individual into another numbered individual,
 * or the change of N value to a new one,
 * or the change of S value to a new one. *)
type event = Birth of int | Death of int*int | Nchange of float | Schange of float


(** The list of all events, including birth times, death times, change of N and change of S,
 * as found in an [array_individuals], [listN] and [listS].
 * The values of [mu] and [nb_alleles] are needed to fill in information on each interval. *)
let init_all_events array_individuals nb_alleles listN listS mu g tor =
  let empty_laleft = Array.make nb_alleles [] in

  let get_birth i (bi, hi, oi, ai) = (bi, Birth(i)) in
  let birth_events = List.mapi get_birth (Array.to_list array_individuals) in

  let get_death i (bi, hi, oi, ai) = (hi, Death(i, oi)) in
  let death_events = List.mapi get_death (Array.to_list array_individuals) in

  let get_S (t,s) = (t, Schange(s)) in
  let schange_events = List.map get_S listS in

  let get_N (t,n) = (t, Nchange(n)) in
  let nchange_events = List.map get_N listN in

  (* we implement the following order on events *)
  let compare_events (t1,e1) (t2,e2) =
    let c = compare t1 t2 in
    if c <> 0 then c
    else
      match e1, e2 with
      (* Schange are smaller than everything else *)
      |Schange(s1), Schange(s2) -> compare s1 s2
      |Schange(_), _ -> -1
      (* Nchange are bigger than Schange and smaller than everything else *)
      |Nchange(_), Schange(_) -> 1
      |Nchange(n1), Nchange(n2) -> compare n1 n2
      |Nchange(_), _ -> -1
      (* Birth are bigger than Schange and Nchange but smaller than Deaths *)
      |Birth(_), Schange(_) -> 1
      |Birth(_), Nchange(_) -> 1
      |Birth(i1), Birth(i2) -> compare i1 i2
      |Birth(_), Death(_) -> -1
      (* Death are bigger than everything else *)
      |Death(n1,o1), Death(n2,o2) -> compare n1 n2
      |Death(_), _ -> 1 in
  (* first concatenate all lists *)
  let all_events = List.rev_append birth_events (List.rev_append death_events (List.rev_append nchange_events schange_events)) in
  (* then order the result *)
  let all_events = List.sort compare_events all_events in 

  (* compute the current N, S, nb_tot in different intervals, 
   * i.e. quantities that need to be computed from the left *)
  let propagate_N_and_S (currN, currS, nb_tot) (t,e) =
    match e with
    |Nchange(n) -> (n, currS, nb_tot), (t, e, n, currS, nb_tot)
    |Schange(s) -> (currN, s, nb_tot), (t, e, currN, s, nb_tot)
    |Birth(_) -> (currN, currS, nb_tot+1), (t, e, currN, currS, nb_tot+1)
    |Death(_) -> (currN, currS, nb_tot-1), (t, e, currN, currS, nb_tot-1) in
  let acc, all_events = fold_left_map propagate_N_and_S (0.,0.,0) all_events in

  (* compute the end point of each interval, a quantity that needs to be folded from right to left *)
  let _ = 
    let (max_t, _, _, _, _) = List.hd (List.rev all_events) in
    if max_t > tor then failwith "The time of origin should be greater than all other pre-specified times." in
  let propagate_tend tend (tstart,e,currN,currS,nb_tot) = tstart, (tstart, tend, e, currN, currS, nb_tot) in
  let acc, all_events = fold_left_map propagate_tend tor (List.rev all_events) in
  let all_events = List.rev all_events in

  (* and finally compute all remaining quantities *)
  let add_other_info = function
    |(tstart, tend, event_start, currN, currS, nb_tot) ->
        let deltat = tend -. tstart in
        let p_no_coal = 
          if currN = 0. && deltat = 0. then 1.
          else exp (-. deltat /. (g*.currN)) in
        let tot_rate = 
          if currN = 0. && nb_tot = 0 then mu
          else mu +. (float_of_int nb_tot) /. (g*.currN) in
        let p_t_not_in_interval = exp (-. tot_rate *. deltat ) in
        (tstart, tend, event_start, p_no_coal, p_t_not_in_interval, tot_rate, currN, currS, nb_tot, (Array.copy empty_laleft)) in
  List.map add_other_info all_events
  

(** The list of weighted intervals for individual [ni] obtained by
 * exploring the list of [all_events] with knowledge of [array_individuals],
 * [array_alleles] and [mu]. *)
let get_intervals ni array_individuals array_alleles all_events mu g =

  (* we take basic information on the focal individual
   * as well as mi and Mi (here called zi). *)
  let (bi, hi, oi, ai) = array_individuals.(ni) in
  let is_mutator = (oi = ni) in
  let individuals_of_ai = array_alleles.(ai) in
  let rec get_mi_and_zi (temp_mi, temp_zi) = function
    |[] -> (temp_mi, temp_zi)
    |nj::tl ->
      let (_, hj, oj, _) = array_individuals.(nj) in
      let new_mi =
        if nj <> ni && oj = ni && hj > temp_mi then hj
        else temp_mi in
      let new_zi =
        if hj > temp_zi then hj
        else temp_zi in
      get_mi_and_zi (new_mi, new_zi) tl in
  let mi, zi  = get_mi_and_zi (bi, hi) individuals_of_ai in
  if false then Printf.printf "\n\nBuilding intervals for ni=%i, %b from %f,
  between mi = %f and zi = %f\n" ni is_mutator bi mi zi;
  let zi = if is_mutator then infinity else zi in

  (* then we use all_events to extract the interesting intervals with weights *)
  let rec aux buffer (p_above, tot_weight) = function
    |[] -> buffer, tot_weight
    |(tstart, tend, event_start, p_no_coal, p_t_not_in_interval, tot_rate, currN, currS, nb_tot, laleft)::tl ->
      (* below mi we don't do anything *)  
      if tend <= mi then 
        aux buffer (p_above, tot_weight) tl

      (* above zi we return the result *)
      else if tstart >= zi then
        buffer, tot_weight

      (* in between we compute the weight and record an interval *)
      else
        let nb_ai_left = 
          if is_mutator then 1.
          else float_of_int (List.length (laleft.(ai))) in

        (* when we are below the first possible event, or when the interval is too thin,
         * we don't care to record it and save a bit of computations. *)
        if (nb_ai_left = 0. && tot_weight = 0.) || tstart = tend then
          aux buffer (p_above, tot_weight) tl

        else
          (* below hi, we need to remove one individual from the nb_tot, namely individual ni *)
          let nb_tot, tot_rate, p_t_not_in_interval = 
            if tend <= hi && p_t_not_in_interval > 0. && p_no_coal > 0. then
              let nb_tot = nb_tot - 1 in
              let tot_rate = mu +. (float_of_int nb_tot) /. (g*.currN) in
              let p_t_not_in_interval = p_t_not_in_interval /. p_no_coal in
              nb_tot, tot_rate, p_t_not_in_interval 
            else if tend <= hi then
              let nb_tot = nb_tot - 1 in
              let tot_rate = mu +. (float_of_int nb_tot) /. (g*.currN) in
              nb_tot, tot_rate, p_t_not_in_interval 
            else nb_tot, tot_rate, p_t_not_in_interval in

          (* the weight depends on the number of lineages recorded to the left *)
          let p_success = nb_ai_left /. (g *. currN *. tot_rate) in

          let weight = p_success *. p_above *. (1. -. p_t_not_in_interval) in
          let new_p_above = p_above *. p_t_not_in_interval in
          let new_tot_weight = tot_weight +. weight in
          let new_acc = (new_p_above, new_tot_weight) in
          if compare nan new_tot_weight = 0 then 
            Printf.printf "Here comes a nan ! 
            For a %b with nb_ai_left = %f , nb_tot = %i, p_t_not_in = %f and p_above = %f 
            on %f, %f, i.e. before zi = %f and hi = %f, with p_no_coal = %f\n" 
            is_mutator nb_ai_left nb_tot p_t_not_in_interval p_above tstart tend zi hi p_no_coal;

          let new_buffer = (tstart, tend, nb_tot, (laleft.(ai)), tot_rate, weight)::buffer in
          aux new_buffer new_acc tl in

  aux [] (1., 0.) all_events


(** The list of [all_events] given as input is modified to take into
 * account the replacement of the coalescent history of individual [ni]
 * by a new [hi_prime, oi_prime]. *)
let replace_coal array_individuals (hi_prime, ni, oi_prime) all_events mu g =

  let (bi, hi, oi, ai) = array_individuals.(ni) in
  if false then Printf.printf "We replace hi = %f by hiprime = %f for individual %i \n" hi hi_prime ni;

  let rec aux buffer = function
    |[] -> List.rev buffer
    |(tstart, tend, event_start, p_no_coal, p_t_not_in_interval, tot_rate, currN, currS, nb_tot, laleft)::tl ->
        if compare nan p_t_not_in_interval = 0 then
          Printf.printf "The input p_t_not_in = %f with tot_rate = %f and deltat = %f and tstart = %f and currN = %f \n" p_t_not_in_interval tot_rate (tend -. tstart) tstart currN;

        (* we merge part of this interval with the one below and keep only the part above hi_prime in this interval *)
        if event_start = Death(ni,oi) && hi_prime > tstart && hi_prime < tend then
          let (tstart_prev, tend_prev, event_start_prev, p_no_coal_prev, p_t_not_in_interval_prev, tot_rate_prev, currN_prev, currS_prev, nb_tot_prev, laleft_prev) = List.hd buffer in
          let deltat1 = hi_prime -. tstart_prev in
          let deltat2 = tend -. hi_prime in
          let p_no_coal1 = exp (-. deltat1 /. (g*.currN)) in
          let p_no_coal2 = exp (-. deltat2 /. (g*.currN)) in
          let nb_tot1 = nb_tot_prev in
          let nb_tot2 = nb_tot in
          let tot_rate1 = mu +. (float_of_int nb_tot1) /. (g*.currN) in
          let tot_rate2 = mu +. (float_of_int nb_tot2) /. (g*.currN) in
          let p_t_not_in_interval1 = exp (-. tot_rate1 *. deltat1) in
          let p_t_not_in_interval2 = exp (-. tot_rate2 *. deltat2) in
          let interval1 = (tstart_prev, hi_prime, event_start_prev, p_no_coal1, p_t_not_in_interval1, tot_rate1, currN, currS, nb_tot1, laleft_prev) in
          let interval2 = (hi_prime, tend, Death(ni, oi_prime), p_no_coal2, p_t_not_in_interval2, tot_rate2, currN, currS, nb_tot2, laleft) in
          let new_buffer = interval2::interval1::(List.tl buffer) in
          begin
            if compare nan p_t_not_in_interval1 = 0 then
              Printf.printf "P_t_not_in1 = %f because tot_rate1 = %f and deltat1 = %f \n" p_t_not_in_interval1 tot_rate1 deltat1;
            if compare nan p_t_not_in_interval2 = 0 then
              Printf.printf "P_t_not_in2 = %f because tot_rate2 = %f and deltat2 = %f \n" p_t_not_in_interval2 tot_rate2 deltat2;
            aux new_buffer tl
          end

        (* we merge this interval and the previous one if this one starts with hi *)
        else if event_start = Death(ni,oi) then
          let (tstart_prev, tend_prev, event_start_prev, p_no_coal_prev, p_t_not_in_interval_prev, tot_rate_prev, currN_prev, currS_prev, nb_tot_prev, laleft_prev) = List.hd buffer in
          let deltat = tend -. tstart_prev in
          let p_no_coal = exp (-. deltat /. (g*.currN)) in
          let nb_tot = 
            if hi_prime > hi then nb_tot + 1
            else nb_tot in
          let tot_rate = mu +. (float_of_int nb_tot) /. (g*.currN) in
          let p_t_not_in_interval = exp (-. tot_rate *. deltat) in
          let interval = (tstart_prev, tend, event_start_prev, p_no_coal, p_t_not_in_interval, tot_rate, currN, currS, nb_tot, laleft_prev) in
          if compare nan p_t_not_in_interval = 0 then
            Printf.printf "P_t_not_in = %f because tot_rate = %f and deltat = %f \n" p_t_not_in_interval tot_rate deltat;
          let new_buffer = interval::(List.tl buffer) in
          aux new_buffer tl

        (* we cut the interval into two pieces if we find hi_prime in the middle *)
        else if hi_prime >= tstart && hi_prime < tend then
          let deltat1 = hi_prime -. tstart in
          let deltat2 = tend -. hi_prime in
          let p_no_coal1 = exp (-. deltat1 /. (g*.currN)) in
          let p_no_coal2 = exp (-. deltat2 /. (g*.currN)) in
          let nb_tot1, nb_tot2 =
            if hi_prime <= hi then
              begin
                if nb_tot-1 < 0 then Printf.printf "Replace hi = %f by hiprime = %f (note that bi = %f ) \n" hi hi_prime bi;
                nb_tot, (nb_tot - 1)
              end
            else
              (nb_tot + 1), nb_tot in
          let tot_rate1 = mu +. (float_of_int nb_tot1) /. (g*.currN) in
          let tot_rate2 = mu +. (float_of_int nb_tot2) /. (g*.currN) in
          let p_t_not_in_interval1 = exp (-. tot_rate1 *. deltat1) in
          let p_t_not_in_interval2 = exp (-. tot_rate2 *. deltat2) in
          let laleft2 = Array.copy laleft in
          let interval1 = (tstart, hi_prime, event_start, p_no_coal1, p_t_not_in_interval1, tot_rate1, currN, currS, nb_tot1, laleft) in
          let interval2 = (hi_prime, tend, Death(ni, oi_prime), p_no_coal2, p_t_not_in_interval2, tot_rate2, currN, currS, nb_tot2, laleft2) in
          let new_buffer = interval2::interval1::buffer in
          begin
            if compare nan p_t_not_in_interval1 = 0 then
              Printf.printf "P_t_not_in1 = %f because tot_rate1 = %f and deltat1 = %f \n" p_t_not_in_interval1 tot_rate1 deltat1;
            if compare nan p_t_not_in_interval2 = 0 then
              Printf.printf "P_t_not_in2 = %f because tot_rate2 = %f and deltat2 = %f \n" p_t_not_in_interval2 tot_rate2 deltat2;
            if tstart >= bi then laleft.(ai) <- ni::(laleft.(ai));
            aux new_buffer tl
          end

        (* otherwise, we keep the same intervals, possibly with different nb_tot, tot_rate, p_t_not_in_interval, laleft... *)
        else
          let new_interval =
            if tend <= hi && tend <= hi_prime then 
              begin
                if tstart >= bi then laleft.(ai) <- ni::(laleft.(ai));
                (tstart, tend, event_start, p_no_coal, p_t_not_in_interval, tot_rate, currN, currS, nb_tot, laleft)
              end

            else if tend > hi && tend <= hi_prime then
              let nb_tot = nb_tot + 1 in
              let p_t_not_in_interval = p_t_not_in_interval *. p_no_coal in
              let tot_rate = tot_rate +. 1. /. (g*.currN) in
              begin
                if compare nan p_t_not_in_interval = 0 then
                  Printf.printf "P_t_not_in = %f because p_no_coal = %f \n" p_t_not_in_interval p_no_coal;
                if tstart >= bi then laleft.(ai) <- ni::(laleft.(ai));
                (tstart, tend, event_start, p_no_coal, p_t_not_in_interval, tot_rate, currN, currS, nb_tot, laleft)
              end

            else if tend > hi && tend > hi_prime then
              (tstart, tend, event_start, p_no_coal, p_t_not_in_interval, tot_rate, currN, currS, nb_tot, laleft)

            else (*if tend <= hi && tend > hi_prime then*)
              let nb_tot = nb_tot - 1 in
              let p_t_not_in_interval = 
                if p_t_not_in_interval > 0. && p_no_coal > 0. then 
                  p_t_not_in_interval /. p_no_coal 
                else 
                  p_t_not_in_interval in
              if compare nan p_t_not_in_interval = 0 then
                Printf.printf "P_t_not_in = %f p_no_coal = %f \n" p_t_not_in_interval p_no_coal;
              let tot_rate = tot_rate -. 1. /. (g*.currN) in
              (tstart, tend, event_start, p_no_coal, p_t_not_in_interval, tot_rate, currN, currS, nb_tot, laleft) in

          aux (new_interval::buffer) tl in
 
  begin
    array_individuals.(ni) <- (bi, hi_prime, oi_prime, ai);
    aux [] all_events
  end


(** A coalescent history obtained by updating the coalescent history 
 * [ncycles] times, using in turns the distribution of each individual's
 * coalescent history conditional on the remaining. *)
let update_past_coal mu g array_individuals array_alleles all_events ncycles = 
  
  (* auxiliary function to update only the history of one leaf ni *)
  let update_one_coal all_events ni =

    let (bi, hi, oi, ai) = array_individuals.(ni) in

    (* builds the list of interesting intervals for this individual and sample one. *)
    let intervals, tot_weight = get_intervals ni array_individuals array_alleles all_events mu g in
    let random = Random.float tot_weight in
    let (ts, te, nb_tot, lineages_ai_left, tot_rate, w) = find_back_interval random intervals in

    (* simulates the death time within the interval. *)
    let hi_prime = ts +. simExp_cond_interval tot_rate 0. (te-.ts) in

    let oi_prime = 
      if oi = ni then ni
      else 
        let nb_ai_left = List.length lineages_ai_left in 
        if nb_ai_left = 0 then failwith "We shouldn't be able to select an interval without friends on which to coalesce"
        else List.nth lineages_ai_left (Random.int nb_ai_left) in

    (* and finally put the new one in the history, and remove the old one. *)
    replace_coal array_individuals (hi_prime, ni, oi_prime) all_events mu g in

  (* we cycle through all individuals each time *)
  let nb_indiv = Array.length array_individuals in
  let rec update_cycle all_events = function
    |0, _ -> all_events
    |n, ni ->
        if ni = nb_indiv then 
          let empty_laleft = Array.make (Array.length array_alleles) [] in
          let empty_the_list_laleft (a,b,c,d,e,f,g,h,i,_) = (a,b,c,d,e,f,g,h,i, (Array.copy empty_laleft)) in
          let new_all_events = List.map empty_the_list_laleft all_events in
          update_cycle new_all_events (n-1, 0)
        else 
          let new_all_events = update_one_coal all_events ni in
          update_cycle new_all_events (n, ni+1) in

  update_cycle all_events (ncycles, 0)


(** The array of informations about individuals, built from
 * the list of alleles, where we consider that each allele
 * is an int list of numbered individuals.
 * Individuals are numbered in [samp_history] and [alleles].
 * Alleles are numbered according to their index in [alleles].
 * And the following information is recorded for each individual:
 * - bi: time of birth,
 * - hi: time of death (initialized first with the time of birth),
 * - oi: the output of the event (initialized with i for mutators, 0 otherwise),
 * - ai: number of its allele. *)
let get_info_arrays samp_history alleles =

  let get_sampling_time ni =
    let rec aux = function
    |[] -> failwith "the individual has no sampling time recorded in the sampling history"
    |(tbj, nj)::tl ->
        if nj = ni then tbj else aux tl in
    aux samp_history in

  let get_allele_of ni =
    let rec aux buffer = function
      |[] -> failwith "the individual has not been found in any allele"
      |hd::tl -> 
          match hd with
          |[] -> aux (buffer+1) tl
          |nj::tl_n -> if nj = ni then buffer else aux buffer (tl_n::tl) in
    aux 0 alleles in

  let find_oldest_indiv all =
    let rec aux buffer_indiv buffer_time = function
      |[] -> buffer_indiv
      |nj::tl ->
          let tbj = get_sampling_time nj in
          if tbj > buffer_time then aux nj tbj tl
          else aux buffer_indiv buffer_time tl in
    aux (-1) (-.1.) all in
  
  (* initialize the array of informations *)
  let size = List.length samp_history in
  let array_individuals = Array.make size (0., 0., 0, 0) in

  (* this function modifies in place the array of informations *)
  let rec aux = function
    |ni when ni >= size || ni < 0 -> ()
    |ni -> 
        let bi = get_sampling_time ni in
        let ai = get_allele_of ni in
        let oldest_el = find_oldest_indiv (List.nth alleles ai) in
        let oi = oldest_el in 
        let info = (bi, bi, oi, ai) in
        begin
          array_individuals.(ni) <- info;
          aux (ni+1)
        end in

  let _ = aux 0 in
  let array_alleles = Array.of_list alleles in
  array_individuals, array_alleles


(** The newly simulated population size values,
 * knowing the sampling history, coalescent history,
 * and list of sampling intensity values. *)
let update_listN all_events hyperprior_N mu g =

  let rec update_previous_n lexplored value = function
    |[] -> List.rev lexplored
    |hd::tl ->
        match hd with
        |(tstart, tend, event_start, p_no_coal, p_t_not_in_interval, tot_rate, currN, currS, nb_tot, laleft) ->
            let currN = value in
            let deltat = tend -. tstart in
            let p_no_coal = exp (-. deltat /. (g*.currN)) in
            let tot_rate = mu +. (float_of_int nb_tot) /. (g*.currN) in
            let p_t_not_in_interval = exp (-. tot_rate *. deltat) in
            match event_start with
            |Nchange(nj) -> 
                let new_hd = (tstart, tend, Nchange(currN), p_no_coal, p_t_not_in_interval, tot_rate, currN, currS, nb_tot, laleft) in
                List.rev_append (new_hd::lexplored) tl
            |_ -> 
                let new_hd = (tstart, tend, event_start, p_no_coal, p_t_not_in_interval, tot_rate, currN, currS, nb_tot, laleft) in
                update_previous_n (new_hd::lexplored) value tl in

  let lambda, chi, psi = hyperprior_N in

  (* here we count the number of birth events,
   * and the sum of N * deltaN,
   * over the interval deltaS. *)
  let rec aux buffer sum_lambda sum_chi sum_psi = function
    |[] -> 
        (* when reaching the end, the last N value should be updated *)
        let new_N_value = simGIG (lambda +. float_of_int sum_lambda) (chi+.sum_chi) (psi+.sum_psi) in
        (*
        let new_N_value = 1. in
        *)
        let new_buffer = update_previous_n [] new_N_value buffer in
        List.rev new_buffer

    |hd::tl ->
        match hd with
        |(tstart, tend, Nchange(nj), p_no_coal, p_t_not_in_interval, tot_rate, currN, currS, nb_tot, laleft) ->
            (* when reaching a time of change, we update the Nj before and re-initialize the counters *)
            let new_N_value = simGIG (lambda +. float_of_int sum_lambda) (chi+.sum_chi) (psi+.sum_psi) in
            let new_buffer = update_previous_n [] new_N_value buffer in
            let deltat = 
              if tend = infinity then 0.
              else tend -. tstart in
            let new_sum_lambda = 0 in
            let new_sum_chi = (float_of_int (nb_tot * (nb_tot-1))) *. deltat /. g in
            let new_sum_psi = 2. *. currS *. deltat in
            aux (hd::new_buffer) new_sum_lambda new_sum_chi new_sum_psi tl

        |(tstart, tend, event_start, p_no_coal, p_t_not_in_interval, tot_rate, currN, currS, nb_tot, laleft) ->
            let deltat = 
              if tend = infinity then 0.
              else tend -. tstart in
            let new_sum_lambda = 
              match event_start with
              |Birth(_) -> sum_lambda + 1
              |Death(nj,oj) when nj <> oj -> sum_lambda - 1
             |_ -> sum_lambda in
            let new_sum_chi = sum_chi +. (float_of_int (nb_tot * (nb_tot-1))) *. deltat /. g in
            let new_sum_psi = sum_psi +. 2. *. currS *. deltat in
            aux (hd::buffer) new_sum_lambda new_sum_chi new_sum_psi tl in

  aux [] 0 0. 0. all_events


(** The newly simulated sampling intensity values,
 * knowing the sampling history,
 * and list of population size values. *)
let update_listS all_events hyperprior_S =

  let rec update_previous_s lexplored value = function
    |[] -> List.rev lexplored
    |hd::tl ->
        match hd with
        |(tstart, tend, Schange(sj), p_no_coal, p_t_not_in_interval, tot_rate, currN, currS, nb_tot, laleft) ->
            let new_hd = (tstart, tend, Schange(value), p_no_coal, p_t_not_in_interval, tot_rate, currN, value, nb_tot, laleft) in
            List.rev_append (new_hd::lexplored) tl
        |(tstart, tend, event_start, p_no_coal, p_t_not_in_interval, tot_rate, currN, currS, nb_tot, laleft) ->
            let new_hd = (tstart, tend, event_start, p_no_coal, p_t_not_in_interval, tot_rate, currN, value, nb_tot, laleft) in
            update_previous_s (new_hd::lexplored) value tl in

  let alpha, beta = hyperprior_S in

  (* here we count the number of birth events,
   * and the sum of N * deltaN,
   * over the interval deltaS. *)
  let rec aux buffer nb_birth sum_deltaN = function
    |[] -> 
        (* when reaching the end, the last S value should anyway be zero *)
        List.rev buffer

    |hd::tl ->
        match hd with
        |(tstart, tend, Birth(nj), p_no_coal, p_t_not_in_interval, tot_rate, currN, currS, nb_tot, laleft) ->
            let new_sum_deltaN = sum_deltaN +. currN *. (tend -. tstart) in
            let new_nb_birth = nb_birth + 1 in
            aux (hd::buffer) new_nb_birth new_sum_deltaN tl 

        |(tstart, tend, Schange(sj), p_no_coal, p_t_not_in_interval, tot_rate, currN, currS, nb_tot, laleft) ->
            (* when reaching a time of change, we update the Sj before and re-initialize the counters *)
            let new_S_value = simGamma (alpha +. float_of_int nb_birth) (beta +. sum_deltaN) in
            let new_buffer = update_previous_s [] new_S_value buffer in
            let new_nb_birth = 0 in
            let new_sum_deltaN = currN *. (tend -. tstart) in
            aux (hd::new_buffer) new_nb_birth new_sum_deltaN tl

        |(tstart, tend, _, p_no_coal, p_t_not_in_interval, tot_rate, currN, currS, nb_tot, laleft) ->
            let new_sum_deltaN = sum_deltaN +. currN *. (tend -. tstart) in
            aux (hd::buffer) nb_birth new_sum_deltaN tl in

  aux [] 0 0. all_events


(** The newly simulated mutation rate,
 * knowing the sampling and coalescent history,
 * and the number of alleles. *)
let update_mu all_events nb_alleles hyperprior_mu =

  let rec aux curr_sum = function
    |[] -> curr_sum
    |(tstart, tend, event_start, p_no_coal, p_t_not_in_interval, tot_rate, currN, currS, nb_tot, laleft)::tl ->
        let deltat = if tend = infinity then 0. else tend -. tstart in
        let new_sum = curr_sum +. (float_of_int nb_tot) *. deltat in
        aux new_sum tl in
  
  let sum_kdelta = aux 0. all_events in
  let alpha, beta = hyperprior_mu in
  simGamma (alpha +. float_of_int nb_alleles) (beta +. sum_kdelta)


(** The newly simulated generation time,
 * knowing the sampling and coalescent history,
 * and the number of alleles. *)
let update_g all_events nb_alleles nb_samp hyperprior_g =

  let rec aux curr_sum = function
    |[] -> curr_sum
    |(tstart, tend, event_start, p_no_coal, p_t_not_in_interval, tot_rate, currN, currS, nb_tot, laleft)::tl ->
        let deltat = if tend = infinity then 0. else tend -. tstart in
        let new_sum = curr_sum +. 
          if currN = 0. then 0.
          else (float_of_int (nb_tot * (nb_tot - 1))) *. deltat /. (2. *. currN) in
        aux new_sum tl in
  
  let sum_coal_rate = aux 0. all_events in
  let alpha, beta = hyperprior_g in
  simInvGamma (alpha +. float_of_int (nb_samp - nb_alleles)) (beta +. sum_coal_rate)


(** The three lists of interesting stuff that are contained in all_events,
 * i.e. the coal_history, the listN and the listS. *)
let cut_all_events all_events =
  let rec aux (coal_history, listN, listS) = function
  |[] -> 
      List.rev coal_history, List.rev listN, List.rev listS
  |(ts, te, Death(ni,oi), _, _, _, _ , _, _, _)::tl -> 
      aux (((ts,ni,oi)::coal_history), listN, listS) tl
  |(ts, te, Nchange(nextN), _, _, _, _ , _, _, _)::tl -> 
      aux (coal_history, ((ts,nextN)::listN), listS) tl
  |(ts, te, Schange(nextS), _, _, _, _ , _, _, _)::tl -> 
      aux (coal_history, listN, ((ts,nextS)::listS)) tl
  |_::tl -> 
      aux (coal_history, listN, listS) tl in
  aux ([],[],[]) all_events


(** Initialization of a list of all_events from the
 * knowledge of an allelic partition, and from the
 * hyperpriors. *)
let gibbs_sampling_init array_individuals array_alleles hyperpriors tchange_N tchange_S =

  (* First, initialize all variables *)
  let hyperprior_N, hyperprior_S, hyperprior_mu, hyperprior_g, tor = hyperpriors in
  let listN = init_listN hyperprior_N tchange_N in
  let listS = init_listS hyperprior_S tchange_S in
  let mu = init_mu hyperprior_mu in
  let g = init_g hyperprior_g in

  (* and finally the list of all_events *)
  let nb_alleles = Array.length array_alleles in
  let all_events_init = init_all_events array_individuals nb_alleles listN listS mu g tor in

  all_events_init, mu, g


(** Perform a Gibbs sampling on all_events with the specified hyperpriors
 * on N, S, mu. [sampling_params] specifies, in this order, the number of
 * steps, the number of steps in the burnin, and the number of steps between
 * two records. Recording is performed throughout the MCMC in the [folder]. *)
let gibbs_sampling init_state array_individuals array_alleles hyperpriors sampling_params folder is_verbose is_first_run =

  let hyperprior_N, hyperprior_S, hyperprior_mu, hyperprior_g, tor = hyperpriors in
  let ninit, nend, nbetween = sampling_params in
  let nsteps = nend - ninit in

  (* Count the basic caracteristics of this dataset *)
  let nb_alleles = Array.length array_alleles in
  let nb_samp = Array.length array_individuals in

  (* In verbose mode, write the size of the dataset *)
  if is_verbose then 
    Printf.printf "\nRunning the Gibbs sampler on a dataset with %i samples and %i alleles for %i steps.\n" 
    nb_samp nb_alleles nsteps;

  (* open the folder where the recording will take place, and remember previous position *)
  let first_dir = Unix.getcwd () in
  let _ = 
    begin
      Unix.chdir folder;
      (* records the prior and data in a standard way *)
      if is_first_run then
        begin
          output_prior hyperpriors;
          output_sampling_truth array_individuals
        end
    end in

  (* Open a bunch of files in our recording folder to write outputs throughout the MCMC *)
  let mode = Open_wronly::Open_append::Open_creat::Open_text::[] in
  let perm = 0o777 in
  let custom_open_out = open_out_gen mode perm in
  let out_log = custom_open_out "log.csv" in
  let out_posterior_N = custom_open_out "posterior_N.csv" in
  let out_posterior_S = custom_open_out "posterior_S.csv" in
  let out_posterior_mu = custom_open_out "posterior_mu.csv" in
  let out_posterior_g = custom_open_out "posterior_g.csv" in
  let out_posterior_coal_times = custom_open_out "posterior_coal_times.csv" in
  let out_posterior_coal_actions = custom_open_out "posterior_coal_actions.csv" in

  (* start by writing the times of change of S and N as a first line in their files *)
  let all_events_init, _, _ = init_state in
  let _, listN, listS = cut_all_events all_events_init in
  let _ = 
    if is_first_run then
      begin
        output_string out_log ("step,t_update_coal,t_update_N,t_update_S,t_update_mu,t_update_g\n");
        output_string out_posterior_S ((string_of_listx true listS)^"\n");
        output_string out_posterior_N ((string_of_listx true listN)^"\n")
      end in

  let rec run_the_chain time_spent curr_state  = function

    (* when reaching the end of the MCMC *)
    |n when n >= nend -> 
        let t1, t2, t3, t4, t5 = time_spent in
        begin
          (* in verbose mode, write the time spent *)
          if is_verbose then
            Printf.printf "Total amount of time spent on different Gibbs updates: %f seconds.\n" (t1+.t2+.t3+.t4+.t5);

          (* close all output files *)
          close_out out_log;
          close_out out_posterior_N;
          close_out out_posterior_S;
          close_out out_posterior_mu;
          close_out out_posterior_g;
          close_out out_posterior_coal_times;
          close_out out_posterior_coal_actions;

          (* go back to the first firectory *)
          Unix.chdir first_dir;

          (* and return the last state visited with the total time *)
          curr_state, time_spent
        end

    (* one step consists in changing in turn listN, listS, mu, g, and the coal_history,
     * and performing the recording if necessary. *)
    |n ->
        let _ = Random.init n in
        (* updates based on conditional distributions *)
        let all_events, mu, g = curr_state in
        let time0 = Unix.gettimeofday () in

        let new_all_events = 
          if nb_samp = 0 then all_events
          else update_past_coal mu g array_individuals array_alleles all_events 1 in
        let time1 = Unix.gettimeofday () in

        let new_mu = update_mu new_all_events nb_alleles hyperprior_mu in
        let time2 = Unix.gettimeofday () in

        let new_g = update_g new_all_events nb_alleles nb_samp hyperprior_g in
        let time3 = Unix.gettimeofday () in

        let new_all_events = update_listS new_all_events hyperprior_S in
        let time4 = Unix.gettimeofday () in

        (* it is very important that N is updated at the very end, because many quantities in all_events are recomputed
         * when updating the new N values, and it thus takes into account the new values of mu and g there. *)
        let new_all_events = update_listN new_all_events hyperprior_N new_mu new_g in
        let time5 = Unix.gettimeofday () in

        (* sum the time spent from the beginning *)
        let (t1, t2, t3, t4, t5) = ((time1-.time0), (time2-.time1), (time3-.time2), (time4-.time3), (time5-.time4)) in
        let new_time_spent =
          match time_spent with
          |(s1, s2, s3, s4, s5) -> (t1 +. s1, t2 +. s2, t3 +. s3, t4 +. s4, t5 +. s5) in

        let _ = 
          begin
            (* the recording depends on 3 sampling parameters given as input *)
            if n mod nbetween = 0 then
              let coal_history, listN, listS = cut_all_events new_all_events in
              begin
                output_string out_log 
                  ((string_of_int n)^","^(string_of_float t1)^","^(string_of_float t5)^","^(string_of_float t4)^","^(string_of_float t2)^","^(string_of_float t3)^"\n");
                output_string out_posterior_N ((string_of_listx false listN)^"\n");
                output_string out_posterior_S ((string_of_listx false listS)^"\n");
                output_string out_posterior_mu ((string_of_float new_mu)^"\n");
                output_string out_posterior_g ((string_of_float new_g)^"\n");
                output_string out_posterior_coal_times ((string_of_coal_history true coal_history)^"\n");
                output_string out_posterior_coal_actions ((string_of_coal_history false coal_history)^"\n")
              end;

            (* printing an estimation of the total time it will take after a few steps *)
            if n - ninit + 1 = 5 && is_verbose then
              let (s1, s2, s3, s4, s5) = new_time_spent in
              let t = (s1 +. s2 +. s3 +. s4 +. s5) /. 5. *. float_of_int (nsteps - n) in
              Printf.printf "The MCMC is expected to reach the end step in approx. %.3f seconds.\n" t
          end in

        (* and we go for another step *)
        run_the_chain new_time_spent (new_all_events, new_mu, new_g) (n+1) in

  run_the_chain (0.,0.,0.,0.,0.) init_state ninit


let gibbs_sampling_resume folder n_more_steps nbetween is_verbose =

  (* First, get back all information contained in the folder *)
  let hyperpriors = read_in_prior folder in
  let _, _, _, _, tor = hyperpriors in
  let listN = read_in_last_listx false folder in
  let listS = read_in_last_listx true folder in
  let mu = read_in_last_mu folder in
  let g = read_in_last_g folder in
  let coal_times, coal_actions = read_in_last_coal_history folder in
  let samp_history, ais = read_in_truth folder in
  let last_known_step = read_in_last_n folder in

  (* second, build back the array of individuals *)
  let array_individuals = 

    let rec build_back_array_individuals buffer = function
      |[],[],[],[] -> 
          Array.of_list (List.rev buffer)
      |bi::tlb, hi::tlh, oi::tlo, ai::tla ->
          let new_buffer = (bi,hi,oi,ai)::buffer in
          build_back_array_individuals new_buffer (tlb,tlh,tlo,tla)
      |_ -> failwith "Error when building back the array of individuals: the list of bi,hi,oi and ai do not have the same length." in

    build_back_array_individuals [] (samp_history, coal_times, coal_actions, ais) in

  (* the array of alleles is a bit more tedious to build back *)
  let array_alleles =
  (* first we need the number of alleles, which should be equal to the maximum in the list of ai's *)
    let rec get_max buffer = function
      |[] -> buffer
      |hd::tl -> if hd > buffer then get_max hd tl else get_max buffer tl in
    let nb_alleles = 1 + (get_max 0 ais) in

    (* second we need an auxiliary function looking for all individuals in a given allele *)
    let get_indiv_in_allele a = 
      let rec aux buffer = function
        |[] -> List.rev buffer
        |ai::tl -> 
            let new_buffer =
              if ai = a then a::buffer
              else buffer in
            aux new_buffer tl in
      aux [] ais in

    (* then a function building the list of alleles *)
    let rec fill_in_list_alleles = function
      |i when i > nb_alleles -> []
      |i ->
          (get_indiv_in_allele i) :: (fill_in_list_alleles (i+1)) in

    (* which we transform into an array *)
    Array.of_list (fill_in_list_alleles 0) in

  (* build the list of all_events *)
  let nb_alleles = Array.length array_alleles in
  let all_events_init = init_all_events array_individuals nb_alleles listN listS mu g tor in

  (* and finally launch the MCMC *)
  let init_state = all_events_init, mu, g in
  let sampling_params = (last_known_step+1, last_known_step+1+n_more_steps, nbetween) in
  gibbs_sampling init_state array_individuals array_alleles hyperpriors sampling_params folder is_verbose false


