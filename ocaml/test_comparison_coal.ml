open Out
open Rvariable
open Allele
open Coalescent

(* gets the coalescent times only, from the coalescent history *)
let get_coaltimes coal_history =
  let aux (t,_,_) = t in
  List.map aux coal_history

(* gets the list of indicators saying whether an event is a mutation or not, from the coalescent history *)
let get_eta coal_history =
  let aux (_,n,a) = 
    if a = n then true else false in
  List.map aux coal_history

(* Please find below a list of a few algorithms that one might want to test.
 * These should all take as input mu, listN and alleles_birthtime.
 * They should output two lists:
 * - a float list of death times in increasing order.
 * - a bool list of "is_a_mutation" values in the same order *)

let algo_gibbs mu g listN alleles_birthtime =
  (* numbering of the individuals according to their birth time in decreasing order *)
  let samp_history = get_samp_history alleles_birthtime in
  (* the allele partition with individual numbers instead of individual birth times *)
  let alleles = get_numbers_in_alleles samp_history alleles_birthtime in
  let array_individuals, array_alleles = get_info_arrays samp_history alleles in
  (* then initialize the coalescent history somewhere random and perform ncycles of gibbs sampling *)
  let listS = (0.,0.)::[] in
  let nb_alleles = List.length alleles_birthtime in
  let all_events_init = init_all_events array_individuals nb_alleles listN listS mu g infinity in
  let ncycles = 50 in
  let all_events = update_past_coal mu g array_individuals array_alleles all_events_init ncycles in
  let rec get_what_we_want coaltimes etalist = function
    |[] -> (List.rev coaltimes), (List.rev etalist)
    |(t, _, Death(ni,oi), _, _, _, _, _, _, _)::tl ->
        let eta = (ni = oi) in
        get_what_we_want (t::coaltimes) (eta::etalist) tl
    |hd::tl -> get_what_we_want coaltimes etalist tl in
  get_what_we_want [] [] all_events

let algo_vertical_reject mu g listN alleles_birthtime = 
  let coal_history = sim_coal_cond_alleles false mu g listN alleles_birthtime in
  get_coaltimes coal_history, get_eta coal_history

let algo_horizontal_reject mu g listN alleles_birthtime = 
  let coal_history = sim_coal_cond_alleles true mu g listN alleles_birthtime in 
  get_coaltimes coal_history, get_eta coal_history

let algo_kingman_optimized mu g listN alleles_birthtime =
  (* the necessary initialization for the Kingman simulation *)
  let list_of_alleles_sizes = List.map List.length alleles_birthtime in
  let narr = Array.of_list list_of_alleles_sizes in
  let theta = 2. *. mu in
  sim_kingman_cond_allele theta narr


(** Comparison of two different algorithms
 * for recovering the coalescent times, given everything else
 * is fixed. This function performs the simulations and records
 * the resulting distribution of coal times and mutations happening
 * in two separate files for each algorithm. *)
let perform_simulations algo1 algo2 mu g listN alleles_birthtime nsamples =

  let rec aux record_coallist1 record_coallist2 record_etalist1 record_etalist2 = function
    |0 -> record_coallist1, record_coallist2, record_etalist1, record_etalist2
    |i ->
      (* the simulation through a rejection algorithm first *)
      let coaltimes1, eta1 = algo1 mu g listN alleles_birthtime in
      let coaltimes2, eta2 = algo2 mu g listN alleles_birthtime in

      let record_coallist1 = coaltimes1::record_coallist1 in
      let record_coallist2 = coaltimes2::record_coallist2 in
      let record_etalist1 = eta1::record_etalist1 in
      let record_etalist2 = eta2::record_etalist2 in

      (* and finally simulate more *)
      aux record_coallist1 record_coallist2 record_etalist1 record_etalist2 (i-1) in
  
  (* perform the simulation and write in files *)
  let record_coallist1, record_coallist2, record_etalist1, record_etalist2 = aux [] [] [] [] nsamples in
  let out_record_coallist1 = open_out ("../simulation-outputs/test_comparison_coal/coal_times_algo1.csv") in
  let out_record_coallist2 = open_out ("../simulation-outputs/test_comparison_coal/coal_times_algo2.csv") in
  let out_record_etalist1 = open_out ("../simulation-outputs/test_comparison_coal/etalist_algo1.csv") in
  let out_record_etalist2 = open_out ("../simulation-outputs/test_comparison_coal/etalist_algo2.csv") in
  begin
    output_string out_record_coallist1 (string_of_list (string_of_list string_of_float ",") "\n" record_coallist1);
    output_string out_record_coallist2 (string_of_list (string_of_list string_of_float ",") "\n" record_coallist2);
    output_string out_record_etalist1 (string_of_list (string_of_list string_of_bool ",") "\n" record_etalist1);
    output_string out_record_etalist2 (string_of_list (string_of_list string_of_bool ",") "\n" record_etalist2);
    close_out out_record_coallist1;
    close_out out_record_coallist2;
    close_out out_record_etalist1;
    close_out out_record_etalist2;
  end

(*
  Pick one of the following alleles for testing first.
  let alleles_birthtime = (0.::0.::[])::(0.::[])::(0.::[])::(0.::[])::[] in
  let alleles_birthtime = (0.::0.::0.::[])::(0.::0.::[])::[] in
  let alleles_birthtime = (0.::0.::0.::0.::0.::0.::[])::(1.::[])::[] in
  let alleles_birthtime = (0.::[])::(0.1::[])::(1.::[])::(1.1::[])::[] in
  let alleles_birthtime = (0.::0.::0.::1.::[])::(1.::[])::[] in
  let alleles_birthtime = (0.::0.::1.::2.::[])::(1.::1.::2.::[])::[] in
  let alleles_birthtime = (0.::0.::0.::0.::0.::0.::[])::(1.::[])::[] in
  let alleles_birthtime = (0.::0.::0.::[])::(0.::0.::0.::[])::[] in
  let alleles_birthtime = (0.::1.::2.::[])::[] in
  let alleles_birthtime = (0.::[])::(0.203::[])::(0.253::[])::[] in
  let alleles_birthtime = (0.::1.1::[])::(1.2::1.3::[])::(2.::[])::[] in
  let alleles_birthtime = (0.::0.201::[])::(0.002::0.203::[])::[] in
  let alleles_birthtime = (0.::0.001::[])::(0.002::0.003::0.004::[])::[] in
  let alleles_birthtime = (0.::0.01::0.02::0.03::0.04::1.05::[])::(1.06::[])::[] in
  let alleles_birthtime = (1.::[])::(0.::[])::(0.::[])::(0.::[])::(0.::[])::(0.::[])::[] in
  let alleles_birthtime = (0.::0.2::0.3::0.9::[])::(0.2::0.2::0.3::0.5::[])::(0.::[])::[] in
  let alleles_birthtime = (0.::0.001::[])::(0.002::0.203::[])::(0.502::[])::[] in
  let alleles_birthtime = (0.::0.2::0.2::0.2::0.3::0.4::[])::(0.::[])::[] in
  let alleles_birthtime = (0.::0.::0.::0.::0.::0.::1.::[])::(1.::1.::1.::1.::[])::[] in
  let alleles_birthtime = (0.::0.1::0.3::[])::(0.5::0.8::[])::[] in
  let alleles_birthtime = (0.::0.::0.::[])::(0.::0.::[])::[] in
  let alleles_birthtime = (0.::0.2::0.2::0.2::0.3::0.4::[])::(0.6::0.8::[])::[] in
  let alleles_birthtime = (0.::0.2::0.2::[])::(0.2::0.2::[])::[] in
*)

(* Then choose the parameters of the simulations *)
let _ =
  let _ = Random.init 5 in
  let listN = (0., 1.)::[] in
  let mu = 1.5 in
  let g = 3.1 in
  let alleles_birthtime = (0.::0.2::0.5::[])::(0.3::0.7::[])::[] in
  perform_simulations algo_horizontal_reject algo_gibbs mu g listN alleles_birthtime 10000;;


