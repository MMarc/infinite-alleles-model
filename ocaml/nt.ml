type nt = A | T | C | G | Melange of nt*nt 

exception UnknownNtLetter

(** A boolean value indicating whether two nucleotides overlap (1) or not (0). *)
let rec is_overlapping nt1 nt2 =
  match nt1, nt2 with
  | Melange (nt3, nt4), _ -> 
      is_overlapping nt3 nt2 || is_overlapping nt4 nt2
  | _, Melange(_, _) -> is_overlapping nt2 nt1
  |_,_ -> nt1 = nt2

(** A boolean indicating whether nucleotide [nt1] is contained within nucleotide [nt2]. *)
let rec is_contained nt1 nt2 =
  match nt1, nt2 with
  | Melange(nt5, nt6), _ -> 
      is_contained nt5 nt2 && is_contained nt6 nt2
  | _, Melange(nt3, nt4) -> is_contained nt1 nt3 || is_contained nt1 nt4
  |_,_ -> nt1 = nt2

(** A boolean indicating whether two nucleotides are strictly identical. *)
let is_equal nt1 nt2 = (is_contained nt1 nt2) && (is_contained nt2 nt1)

(** A char corresponding to a nucleotide [nt]. This is standard notation. *)
let char_of_nt nt =
  match nt with
  | A -> 'a'
  | T -> 't'
  | C -> 'c'
  | G -> 'g'
  | _ ->
      if is_equal nt (Melange(A,G)) then 'r'
      else if is_equal nt (Melange(T,C)) then 'y'
      else if is_equal nt (Melange(A,T)) then 'w'
      else if is_equal nt (Melange(C,G)) then 's'
      else if is_equal nt (Melange(A,C)) then 'm'
      else if is_equal nt (Melange(G,T)) then 'k'
      else if is_equal nt (Melange(A,Melange(T,C))) then 'h'
      else if is_equal nt (Melange(T,Melange(C,G))) then 'b'
      else if is_equal nt (Melange(A,Melange(C,G))) then 'v'
      else if is_equal nt (Melange(A,Melange(T,G))) then 'd'
      else 'n'

(** A string of length 1 with the standard char corresponding to a given nucleotide [nt]. *)
let string_of_nt nt =
  String.make 1 (char_of_nt nt)

(** A nucleotide corresponding to a given char [c]. Raises an exception [UnknownNtLetter] if the character is not standard. *)
let nt_of_char c =
  if c = 'a' || c = 'A' then A
  else if c = 't' || c = 'T' || c = 'u' || c = 'U' then T
  else if c = 'c' || c = 'C' then C
  else if c = 'g' || c = 'G' then G
  else if c = 'r' || c = 'R' then Melange(A,G)
  else if c = 'y' || c = 'Y' then Melange(T,C)
  else if c = 'w' || c = 'W' then Melange(A,T)
  else if c = 's' || c = 'S' then Melange(C,G)
  else if c = 'm' || c = 'M' then Melange(A,C)
  else if c = 'k' || c = 'K' then Melange(G,T)
  else if c = 'h' || c = 'H' then Melange(A,Melange(T,C))
  else if c = 'b' || c = 'B' then Melange(T,Melange(C,G))
  else if c = 'v' || c = 'V' then Melange(A,Melange(C,G))
  else if c = 'd' || c = 'D' then Melange(A,Melange(T,G))
  else if c = 'n' || c = 'N' then Melange(Melange(A,T),Melange(C,G))
  else raise UnknownNtLetter;;

(** An int value corresponding to each standard nt character. Warning: this is not standard ! *)
let int_of_char c =
  if c = 'a' || c = 'A' then 0
  else if c = 't' || c = 'T' || c = 'u' || c = 'U' then 1
  else if c = 'c' || c = 'C' then 2
  else if c = 'g' || c = 'G' then 3
  else if c = 'r' || c = 'R' then 4
  else if c = 'y' || c = 'Y' then 5 
  else if c = 'w' || c = 'W' then 6
  else if c = 's' || c = 'S' then 7
  else if c = 'm' || c = 'M' then 8
  else if c = 'k' || c = 'K' then 9
  else if c = 'h' || c = 'H' then 10
  else if c = 'b' || c = 'B' then 11
  else if c = 'v' || c = 'V' then 12
  else if c = 'd' || c = 'D' then 13
  else if c = 'n' || c = 'N' then 14
  else raise UnknownNtLetter;;

(** An int value corresponding to a given nucleotide. Warning: this is not standard ! *)
let int_of_nt nt =
  int_of_char (char_of_nt nt)

(** [nt_of_string s] is the nucleotide corresponding to the first character in string [s]. *)
let nt_of_string s = nt_of_char (String.get s 0)

let ntlist_of_string s =
  let rec aux buffer = function
    |i when i = 0 -> List.rev buffer
    |i ->
        let new_buffer = (nt_of_char s.[i-1]) :: buffer
        in aux new_buffer (i-1)
  in aux [] (String.length s)

(** A boolean value indicating whether two nucleotides are different pyrimidines (T,C). *)
let is_transition_pyr nt1 nt2 =
  match nt1, nt2 with
  |T,C -> true
  |C,T -> true
  |_,_ -> false

(** A boolean value indicating whether two nucleotides are different purines (A,G). *)
let is_transition_pur nt1 nt2 =
  match nt1, nt2 with
  |A,G -> true
  |G,A -> true
  |_,_ -> false

(** A boolean indicating whether there is a transition to go from nucleotide [nt1] to nucleotide [nt2]. *)
let is_transition nt1 nt2 =
  is_transition_pur nt1 nt2 || is_transition_pyr nt1 nt2

(** A boolean indicating whether we know there has been a change ([true]) or not ([false]). Be careful here that [false] values only mean that we cannot tell whether there is a mutation or not. For example, [is_mutation A (Melange of A*T)] would be [false]. *)
let is_mutation nt1 nt2 =
  not( is_overlapping nt1 nt2 )
  (* not (nt1 = nt2) *)

let is_transversion nt1 nt2 =
  (not (is_transition nt1 nt2)) && (is_mutation nt1 nt2)

(** Comparison function of two nucleotides.
 * Relies on the transformation of a nt into an int. *)
let compare_nt nt1 nt2 =
  if is_overlapping nt1 nt2 then 0
  else
    if int_of_nt nt1 > int_of_nt nt2 then 1
    else -1

(** The number of undefined nucleotides in the sequence.
 * Note that if the sequence contains an unknownletter, the exception
 * is not raised. Instead, we consider that the whole sequence is composed
 * of undefined nucleotides. *)
let number_undefined_nt seq =
  let rec aux i sum =
  if i >= String.length seq then sum
  else
    match nt_of_char seq.[i] with
    |exception UnknownNtLetter -> String.length seq
    |Melange(_) -> aux (i+1) (sum+1)
    |_ -> aux (i+1) sum
  in aux 0 0

(** The proportion of undefined nucleotides in the sequence. *)
let prop_of_undefined_nt seq =
  let uncertain_sites = number_undefined_nt seq in
  (float_of_int uncertain_sites) /. (float_of_int (String.length seq))


