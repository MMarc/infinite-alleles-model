
(** The sequence named [name] as found in a fasta [infile]. *)
let read_seq infile name =
  let file = open_in infile

  in let rec findseq name =
    let seqname = match input_line file with
    |nameline -> String.sub nameline 1 ((String.length nameline)-1)
    |exception End_of_file -> ""

    in let seq = match input_line file with
    |seqline -> seqline
    |exception End_of_file -> ""

    in if seqname = name then seq
    else if seqname = "" || seq = "" then
      begin
        Printf.printf "The sequence named %s was looked for in file %s.\n" name infile;
        failwith "Error: the sequence cannot be found."
      end
    else findseq name

  in let result = findseq name
  in begin close_in file; result end


(** The first sequence found in a fasta file with sequences cut on multiple lines *)
let read_seq_multiline filename =
  let file = open_in filename in

  (* read lines one by one *)
  let rec aux seq =
    match input_line file with
    |exception End_of_file -> 
        begin
          close_in file;
          seq
        end;
    |s ->
        (* remove annoying special characters like \n *)
        let s = String.trim s in
        (* if we are reading the first title line, we keep looking for a sequence,
         * but if we already have read a sequence and find a title line, we return the seq *)
        if String.get s 0 = '>' then 
          if seq = "" then 
            aux seq
          else
            begin
              close_in file;
              seq
            end
        (* if we are reading a sequence line, we append it to the previously read seq *)
        else
          let new_seq = seq ^ s in
          aux new_seq in

  aux ""


(** The name of a sequence extracted from a line containing the name
 * in a fasta file. *)
let read_name_line nameline =
  String.sub nameline 1 ((String.length nameline)-1)


(** The content of a CSV file [infile] containing metadata
  * information on sequences. *)
let read_in_csv infile =
  let content = ref []
  in let file = open_in infile
  in let rec read_in_one_more_csv_line () =
      let line_content = input_line file
      in let parsed_content = String.split_on_char '\t' line_content
      in let title = List.hd parsed_content
      in let date = List.nth parsed_content 4
      in let region = List.nth parsed_content 5
      in let country = List.nth parsed_content 6
      in let host = List.nth parsed_content 14
      in let date_submitted = List.nth parsed_content 26
      in let new_element = title, date, region, country, host, date_submitted
      in begin 
        content := new_element :: (!content); 
        read_in_one_more_csv_line () 
      end
  in try read_in_one_more_csv_line ()
  with End_of_file ->
    close_in file;
    !content


(** The content of a CSV file [infile] containing metadata
  * information on sequences. *)
let read_in_short_csv infile =
  let content = ref [] in
  let file = open_in infile in
  let rec read_in_one_more_csv_line () =
      let line_content = input_line file
      in let parsed_content = String.split_on_char '\t' line_content
      in let title = List.hd parsed_content
      in let date = List.nth parsed_content 1
      in let new_element = title, date
      in begin 
        content := new_element :: (!content); 
        read_in_one_more_csv_line () 
      end
  in try read_in_one_more_csv_line ()
  with End_of_file ->
    close_in file;
    !content


(** The metadata information in [content] corresponding to the
 * sequence named [name]. *)
let rec find_metadata_tuple name content =
  match content with
  |[] -> ("", "", "", "", "", "")
  |(title,date,region,country,host,datesub)::tl ->
      if title = name then title,date,region,country,host,datesub
      else find_metadata_tuple name tl

(** The metadata information in [content] corresponding to the
 * sequence named [name]. *)
let rec find_metadata_short_tuple name content =
  match content with
  |[] -> ("", "")
  |(title,date)::tl ->
      if title = name then (title,date)
      else find_metadata_short_tuple name tl


(** A boolean indicating whether a string indicating a date in the
 * format yyyy-mm-dd seems roughly valid, i.e. contains yyyy, mm, dd,
 * with not too extreme values. *)
let is_correct_date datestring =
  if String.length datestring <> 10 then false
  else
    let exploded_date = String.split_on_char '-' datestring
    in match exploded_date with
    |yyyy::mm::dd::[] ->
        begin
          try
          let year = int_of_string yyyy
          in let month = int_of_string mm
          in let day = int_of_string dd
          in if year > 0 && month < 13 && month > 0 && day < 32 && day > 0 then true
          else false
        with Failure _ -> false
        end
    |_ -> false


(** A 'a list corresponding to the transformation of a given
 * string, cut at [sep] and to which [type_of_element] is applied. *)
let read_csv_line sep type_of_element line =
  let parsed_content = List.rev (String.split_on_char sep line) in
  let shorten_parsed_content =
    if List.hd parsed_content = "" then List.tl parsed_content
    else parsed_content in
  List.rev_map type_of_element shorten_parsed_content


(** The list corresponding to the first line of a file
 * read by cutting at [sep] and applying [type_of_element]. *)
let read_in_list sep type_of_element filename =
  let file = open_in filename in
  let line_content = input_line file in
  begin
    close_in file;
    read_csv_line sep type_of_element line_content
  end

let read_in_float_list sep filename = read_in_list sep float_of_string filename


(** To float list list corresponding to the content of
 * a file where each line is a float list. 
 * The last line of the file is on top of the list.  *)
let read_in_float_list_list sep filename =
  let file = open_in filename in
  let rec aux record =
    try 
      let line_content = input_line file in
      let float_list = read_csv_line sep float_of_string line_content in
      aux (float_list::record)
    with End_of_file ->
      close_in file;
      record in
  aux []


(** The string corresponding to the last line of a file. *)
let read_in_last_line filename =
  
  let file = open_in filename in

  let rec aux last_line = 
    try
      let line_content = input_line file in
      aux line_content
    with End_of_file ->
      close_in file;
      last_line in
  
  aux ""


(** The float*float list corresponding to the times of change and
 * last values taken by either S [read_s = true] or N, 
 * as recorded in a mcmc output [folder]. *)
let read_in_last_listx read_s folder =

  (* the filename depending on whether we want to read S or N *)
  let filename = 
    if read_s then folder ^ "/posterior_S.csv"
    else folder ^ "/posterior_N.csv" in
  let file = open_in filename in

  (* the first line contains the times of change *)
  let tchange = read_csv_line ',' float_of_string (input_line file) in
  let values = read_csv_line ',' float_of_string (read_in_last_line filename) in

  (* and we build the corresponding listS or listN, i.e. a float*float list *)
  let rec build_tuple_list = function
    |[],[] -> []
    |hd1::tl1, hd2::tl2 -> (hd1, hd2)::(build_tuple_list (tl1,tl2))
    |_ -> failwith "Error while reading a listS or listN file: the first and last line do not have the same number of elements." in
  build_tuple_list (tchange, values)


(** The float corresponding to the last [mu] value
 * as recorded in a mcmc output [folder]. *)
let read_in_last_mu folder =
  let filename = folder ^ "/posterior_mu.csv" in
  float_of_string (read_in_last_line filename)

(** The float corresponding to the last [g] value
 * as recorded in a mcmc output [folder]. *)
let read_in_last_g folder =
  let filename = folder ^ "/posterior_g.csv" in
  float_of_string (read_in_last_line filename)


(** The int corresponding to the last step recorded in the mcmc log. *)
let read_in_last_n folder =
  let filename = folder ^ "/log.csv" in
  let last_line = read_in_last_line filename in
  let parsed_content = String.split_on_char ',' last_line in
  int_of_string (List.hd parsed_content)


(** The coalescent history corresponding to the last record
 * in a mcmc output [folder]. *)
let read_in_last_coal_history folder =

  let filename_times = folder ^ "/posterior_coal_times.csv" in
  let filename_actions = folder ^ "/posterior_coal_actions.csv" in

  let t = read_csv_line ',' float_of_string (read_in_last_line filename_times) in
  let o = read_csv_line ',' int_of_string (read_in_last_line filename_actions) in
  t, o
    

(** The float list corresponding to the sampling times,
 * recorded in the first line of a file *)
let read_in_truth folder =

  let filename_sampling = folder ^ "/truth_sampling_times.csv" in
  let filename_alleles = folder ^ "/truth_alleles.csv" in

  let sampling_times = read_in_list ',' float_of_string filename_sampling in
  let alleles = read_in_list ',' int_of_string filename_alleles in

  sampling_times, alleles


(** The hyperparameters for the Gamma distribution of mu and S,
 * and the GIG distribution of N. *)
let read_in_prior folder =

  (* the filenames *)
  let filename_N = folder ^ "/prior_N.csv" in
  let filename_S = folder ^ "/prior_S.csv" in
  let filename_mu = folder ^ "/prior_mu.csv" in
  let filename_g = folder ^ "/prior_g.csv" in
  let filename_tor = folder ^ "/prior_tor.csv" in

  (* opening them *)
  let file_N = open_in filename_N in
  let file_S = open_in filename_S in
  let file_mu = open_in filename_mu in
  let file_g = open_in filename_g in
  let file_tor = open_in filename_tor in

  (* three floats on separate lines for the params of the GIG distribution *)
  let lambda = float_of_string (input_line file_N) in
  let chi = float_of_string (input_line file_N) in
  let psi = float_of_string (input_line file_N) in
  let hyperprior_N = (lambda, chi, psi) in

  (* two floats on separate lines for the params of the gamma distribution of S *)
  let alpha_S = float_of_string (input_line file_S) in
  let beta_S = float_of_string (input_line file_S) in
  let hyperprior_S = (alpha_S, beta_S) in

  (* same for the params of the Gamma distribution of mu *)
  let alpha_mu = float_of_string (input_line file_mu) in
  let beta_mu = float_of_string (input_line file_mu) in
  let hyperprior_mu = (alpha_mu, beta_mu) in

  let alpha_g = float_of_string (input_line file_g) in
  let beta_g = float_of_string (input_line file_g) in
  let hyperprior_g = (alpha_g, beta_g) in

  let tor = float_of_string (input_line file_tor) in
  (* closing everything *)
  let _ = 
    begin
      close_in file_N;
      close_in file_S;
      close_in file_mu;
      close_in file_g;
      close_in file_tor
    end in

  (* the three tuples in standard ordering *)
  (hyperprior_N, hyperprior_S, hyperprior_mu, hyperprior_g, tor)


