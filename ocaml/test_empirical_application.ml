open Unix
open Coalescent
open Out
open In
open Date

let create_seq_time min max step =
  let rec aux curr =
    if curr > max then []
    else curr::(aux (curr+.step)) in
  aux min

let start () =
  let _ = Random.init 0 in

  (*
  let region_short = "FR" in
  let region_short = "IT" in
  let region_short = "CH" in 
  *)
  let region_short = "DE" in
  let alleles_filename = "../data-transformed/dataset/" ^ region_short ^ "_alleles.csv" in
  let alleles_birthtime = read_in_float_list_list ',' alleles_filename in
  let writing_repo = "../simulation-outputs/test_empirical_application/" ^ region_short in

  (* numbering of the individuals according to their birth time in decreasing order *)
  let samp_history = get_samp_history alleles_birthtime in

  (* the allele partition with individual numbers instead of individual birth times *)
  let alleles = get_numbers_in_alleles samp_history alleles_birthtime in

  (* the two arrays of individuals and alleles *)
  let array_individuals, array_alleles = get_info_arrays samp_history alleles in
  
  (* timeline of N and S *)
  let min_date = 0. in
  (* for S we do something simple, with 5 intervals of 4 weeks each, up until 2020-01-13 *)
  let max_date_S = float_of_int (day_number_between_two_dates (date_of_string "2020-01-13") (date_of_string "2020-06-01")) in
  let tchange_S = create_seq_time min_date max_date_S 28. in
  (* for N, we take 4 intervals of two weeks first, then intervals of one week, and finally intervals of two weeks again *)
  (*
  let max_date_N_1 = float_of_int (day_number_between_two_dates (date_of_string "2020-04-06") (date_of_string "2020-06-01")) in
  let max_date_N_2 = float_of_int (day_number_between_two_dates (date_of_string "2020-01-27") (date_of_string "2020-06-01")) in
  *)
  let max_date_N_3 = float_of_int (day_number_between_two_dates (date_of_string "2019-11-18") (date_of_string "2020-06-01")) in
  let tchange_N = 
    (create_seq_time min_date max_date_N_3 14.) in
    (*
    (create_seq_time min_date max_date_N_1 14.) @  
    (create_seq_time (max_date_N_1+.7.) max_date_N_2 7.) @ 
    (create_seq_time (max_date_N_2+.14.) max_date_N_3 14.)  in
    *)
 
  (* hyperpriors that one might want to change *)
  let hyperprior_N = (0.1, 0., 0.002) in
  let hyperprior_S = (0.1, 100.) in
  let hyperprior_mu = (1., 10.) in
  let g = 5. in
  let tor = max_date_N_3 in
  let hyperpriors = (hyperprior_N, hyperprior_S, hyperprior_mu, g, tor) in

 (* sampling parameters for the mcmc *)
  let ninit = 0 in
  let nend = 10000 in
  let nbetween = 1 in
  let sampling_params = (ninit, nend, nbetween) in

  (* run the MCMC and sample the posterior distribution *)
  let init_state = gibbs_sampling_init array_individuals array_alleles hyperpriors tchange_N tchange_S in
  gibbs_sampling init_state array_individuals array_alleles hyperpriors sampling_params writing_repo true true


let resume () =
  let region_short = "CH" in
  let writing_repo = "../simulation-outputs/test_empirical_application/" ^ region_short in
  let nbetween = 1 in
  let n_more_steps = 10 in
  gibbs_sampling_resume writing_repo n_more_steps nbetween true


let _ = start ()
