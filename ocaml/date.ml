type month = Jan | Feb | Mar | Apr | May | Jun | Jul | Aug | Sep | Oct | Nov | Dec

let int_of_month = function
  |Jan -> 1
  |Feb -> 2
  |Mar -> 3
  |Apr -> 4
  |May -> 5
  |Jun -> 6
  |Jul -> 7
  |Aug -> 8
  |Sep -> 9
  |Oct -> 10
  |Nov -> 11
  |Dec -> 12

let month_of_int n =
  if n = 1 then Jan
  else if n = 2 then Feb
  else if n = 3 then Mar
  else if n = 4 then Apr
  else if n = 5 then May
  else if n = 6 then Jun
  else if n = 7 then Jul
  else if n = 8 then Aug
  else if n = 9 then Sep
  else if n = 10 then Oct
  else if n = 11 then Nov
  else if n = 12 then Dec
  else failwith "Not a valid number for a month"

let string_of_month = function
  |Jan -> "January"
  |Feb -> "February"
  |Mar -> "March"
  |Apr -> "April"
  |May -> "May"
  |Jun -> "June"
  |Jul -> "July"
  |Aug -> "August"
  |Sep -> "September"
  |Oct -> "October"
  |Nov -> "November"
  |Dec -> "December"

type weekday = Mon | Tue | Wed | Thu | Fri | Sat | Sun

let int_of_weekday = function
  |Mon -> 1
  |Tue -> 2
  |Wed -> 3
  |Thu -> 4
  |Fri -> 5
  |Sat -> 6
  |Sun -> 7

let string_of_weekday = function
  |Mon -> "Monday"
  |Tue -> "Tuesday"
  |Wed -> "Wednesday"
  |Thu -> "Thursday"
  |Fri -> "Friday"
  |Sat -> "Saturday"
  |Sun -> "Sunday"

type date = {year:int; month:month; day:int}

(** a negative integer if [d1] is before [d2],
 * zero if the two dates are identical, and
 * a positive integer otherwise. *)
let compare_dates d1 d2 =
  if d1.year = d2.year then
    if d1.month = d2.month then
      if d1.day = d2.day then 0
      else if d1.day < d2.day then (-1)
      else 1
    else if (int_of_month d1.month) < (int_of_month d2.month) then (-1)
    else 1
  else if d1.year < d2.year then (-1)
  else 1

(** Boolean indicating whther a given year is bissextile,
 * i.e. is a leap year. *)
let is_bissextile = function
  |y -> (y mod 4 = 0 && y mod 100 <> 0) || (y mod 400 = 0)

(** Number of days in a given year. *)
let day_number_in_year = function
  |year -> if is_bissextile year then 366 else 365

(** Number of days in a given [year] and [month]. *)
let day_number_in_year_month year = function
  |Jan -> 31
  |Feb -> if is_bissextile year then 29 else 28
  |Mar -> 31
  |Apr -> 30
  |May -> 31
  |Jun -> 30
  |Jul -> 31
  |Aug -> 31
  |Sep -> 30
  |Oct -> 31
  |Nov -> 30
  |Dec -> 31

(** Boolean indicating whether the date is valid, i.e.
 * the date.day is > 0 and less than the number of days in
 * the given date.month and date.year. *)
let is_valid_date date =
  let sup = day_number_in_year_month date.year date.month in
  (date.day > 0 && date.day <= sup)

(** The number of days between a starting date d1 (inclusive)
 * and an ending date d2 (exclusive). *)
let rec day_number_between_two_dates d1 d2 =
  (* if we are given dates in reverse order, we return a negative result *)
  if d1.year > d2.year then - day_number_between_two_dates d2 d1

  else if d1.year = d2.year then
    (* if we are given dates in reverse order, we return a negative result *)
    if (int_of_month d1.month) > (int_of_month d2.month) then - day_number_between_two_dates d2 d1

    else if d1.month = d2.month then
      (* same exact date: 0 days in between,
       * i.e. we exclude here the end date *)
      d2.day - d1.day

    else 
      (* we cut the month of d1 in two pieces: before and after the date d1 *)
      let beginning_of_month = {year=d1.year; month=d1.month; day=1} in
      let next_month = month_of_int ((int_of_month d1.month)+1) in
      let remaining_until_end_of_month = (day_number_in_year_month d1.year d1.month) - (day_number_between_two_dates beginning_of_month d1) in
      let next_date = {year=d1.year; month=next_month; day=1} in
      remaining_until_end_of_month + (day_number_between_two_dates next_date d2)

  else
    (* we cut the year of d1 in two pieces: before and after the date d1 *)
    let beginning_of_year = {year=d1.year; month=Jan; day=1} in
    let remaining_until_end_of_year = (day_number_in_year d1.year) - (day_number_between_two_dates beginning_of_year d1) in
    let next_date = {year=d1.year+1; month=Jan; day=1} in
    remaining_until_end_of_year + (day_number_between_two_dates next_date d2)


let date_of_string datestring =
  let error_msg = "Invalid date or date formatting is not yyyy-mm-dd." in
  let exploded_date = String.split_on_char '-' datestring in
  match exploded_date with
  |yyyy::mm::dd::[] ->
      begin
        try
          let year = int_of_string yyyy in
          let month = month_of_int (int_of_string mm) in
          let day = int_of_string dd in
          let date = {year=year; month=month; day=day} in
          if is_valid_date date then date
          else failwith error_msg
        with Failure _ -> failwith error_msg
      end
  |_ -> failwith error_msg

