open Nt
open Aa
open List_addons

type ntmut = Ntsnp of int*nt | Ntdel of int*int | Ntins of int*nt list

type aamut = Aasnp of int*aa | Aadel of int*int | Aains of int*aa list

exception UnknownMut


let compare_ntlist = compare_list compare_nt

let compare_aalist = compare_list compare_aa

(** Comparison of two aa mutations, with the following rules:
  * - a deletion is bigger than everything else,
  * - an insertion is bigger than an insertion,
  * - otherwise, the event touching the smallest position is the smallest,
  * - in case of equality, we compare nucleotides or sequences being inserted. *)
let compare_ntmut mut1 mut2 =
  match mut1, mut2 with
  |Ntdel(i1,i2), Ntdel(j1,j2) ->
      let comparison = compare i1 j1
      in
        if comparison = 0 then compare i2 j2
        else comparison
  |Ntins(i1,ntl1), Ntins(i2,ntl2) ->
      let comparison = compare i1 i2
      in
        if comparison = 0 then compare_ntlist ntl1 ntl2
        else comparison
  |Ntsnp(i1,nt1), Ntsnp(i2,nt2) ->
      let comparison = compare i1 i2
      in
        if comparison = 0 then compare_nt nt1 nt2
        else comparison
  |Ntdel(_), _ -> 1
  |_, Ntdel(_) -> -1
  |Ntins(_), Ntsnp(_) -> 1
  |Ntsnp(_), Ntins(_) -> -1

(** Comparison of two aa mutations, with the same rules than for the
 * nt mutations. *)
let compare_aamut mut1 mut2 =
  match mut1, mut2 with
  |Aadel(i1,i2), Aadel(j1,j2) ->
      if compare i1 j1 = 0 then compare i2 j2
      else compare i1 j1
  |Aains(i1,aal1), Aains(i2,aal2) ->
      if compare i1 i2 = 0 then compare_aalist aal1 aal2
      else compare i1 i2
  |Aasnp(i1,aa1), Aasnp(i2,aa2) ->
      if compare i1 i2 = 0 then compare_aa aa1 aa2
      else compare i1 i2
  |Aadel(_), _ -> 1
  |_, Aadel(_) -> -1
  |Aains(_), Aasnp(_) -> 1
  |Aasnp(_), Aains(_) -> -1


let compare_ntall = compare_list compare_ntmut

let compare_aaall = compare_list compare_aamut

let compare_compressed_representation (i1, ntmut_list1) (i2, ntmut_list2) =
  if compare i1 i2 = 0 then compare_ntall ntmut_list1 ntmut_list2
  else compare i1 i2


(* Functions keeping track of ordered lists of alleles or related objects. *)

let insert_nt_allele l = insert_generic compare_ntall l

let insert_aa_allele l = insert_generic compare_aaall l

let insert_in_compressed_representation_alleles l = insert_generic compare_compressed_representation l

(* Functions keeping track of ordered lists of snps, 
 * obtained by dispatching alleles within already-existing lists of snps. *)

let dispatch_nt_snps l = dispatch_generic compare_ntmut l

let dispatch_aa_snps l = dispatch_generic compare_aamut l



exception StartNotFound

(** The list of starting positions, in the string nucleotide sequence [seq], of a primer [primer]. 
 * The match of [primer] in the sequence is recorded only if it is good up to a tolerance of 
 * [tol] nucleotides not correct. *)
let rec find_subseq seq primer previous_scores only_one position_list i tol score_imperfect =
  let rec update scores j =
    (* the first position of the score gives the matching score for the whole primer with the seq up to position i *)
    (* the second position of the score gives the matching score for the (primer up to position -1) with the seq up to position i *)
    if j >= (String.length primer) || j < 0 then scores
    else
      let add_score =
        let nti =
          match nt_of_char seq.[i] with
          |n -> n
          |exception UnknownNtLetter -> Printf.printf "issue with nt %c in seq position %d\n" seq.[i] i; raise UnknownNtLetter
        in let ntj =
          match nt_of_char primer.[j] with
          |n -> n
          |exception UnknownNtLetter -> Printf.printf "issue with nt %c in primer position %d\n" primer.[j] j; raise UnknownNtLetter
        in
          if nti = ntj then 1.
          else if is_overlapping nti ntj then score_imperfect
          else 0.
      in match scores with
        (* if we don't already have a list of sufficient length, i.e. at the beginning of the comparison, add an element *)
        |[] -> (add_score) :: (update [] (j-1))
        (* else, just add the matching score of primer[j] with seq[i] and update what follows *)
        |hd::l -> (add_score +. hd) :: (update l (j-1))
  in 
    if i < 0 then find_subseq seq primer [] only_one [] 0 tol score_imperfect
    (* upon reaching the end of the seq, we return the position_list, which can thus be empty !! *)
    else if i >= (String.length seq) then position_list 
    else
      (* we update the scores, taking into account one new position and thus "moving" to the right the sliding window *)
      let new_scores = update previous_scores ((String.length primer) - 1)
      in match new_scores with
      |[] -> failwith "Error in the algorithm for finding a primer in a sequence: this should never happen"
      |hd::l -> 
        (* we return the position of the first time we find the primer, or else remove the first score and keep searching *)
        if String.length primer = List.length new_scores && hd >= (float_of_int (String.length primer)) -. tol then 
          let position = i + 1 - (String.length primer)
          in 
            if only_one then position::position_list
            else find_subseq seq primer l only_one (position::position_list) (i+1) tol score_imperfect
        else find_subseq seq primer l only_one position_list (i+1) tol score_imperfect

(* wrapper of the previous function to get only the first position of the primer. 
 * Note that it is not efficient because we look for the primer in all the sequence but only the first. *)
let find_primer seq primer tol score_imperfect =
  match find_subseq seq primer [] true [] 0 tol score_imperfect with
  |[] -> raise StartNotFound
  |hd::tl -> hd

(* wrapper of find_subseq to get the list of start codon positions *)
let find_start seq =
  find_subseq seq "atg" [] false [] 0 0. 0.

(* wrapper of find_subseq to get the list of stop codon positions *)
let find_stop seq =
  List.sort compare 
    ((find_subseq seq "taa" [] false [] 0 0. 0.) @ 
     (find_subseq seq "tag" [] false [] 0 0. 0.) @ 
     (find_subseq seq "tga" [] false [] 0 0. 0.))

    
(** Return the position in the seq on which we find the first 20 nt of the refseq, with a tolerance of 2 nt nucleotides.
 * Or the opposite of the position of the refseq on which we find the 20 first nt of the seq. *)
let find_matching_position refseq seq =
  let tol = 2. in
  let score_imperfect = 0.8 in
  let primer1 = String.sub refseq 0 20 in
  let subseq = String.sub seq 0 500 in
  match find_primer subseq primer1 tol score_imperfect with
    |i -> i
    |exception StartNotFound ->
        let primer2 = String.sub seq 0 20 in
        let subseqref = String.sub refseq 0 500 in
        - (find_primer subseqref primer2 tol score_imperfect)

   

exception AlleleWithGap


let ntmut_of_string s =
  match String.split_on_char ':' s with
  |el1::el2::el3::[] when el1 = "DEL" -> Ntdel( (int_of_string el2)-1, (int_of_string el3)-1 )
  |el1::el2::el3::[] when el1 = "INS" -> Ntins( (int_of_string el2)-1, ntlist_of_string el3 )
  |e::[] ->
      let pos = int_of_string (String.sub e 1 ((String.length s) - 2))
      in let nt2 = nt_of_char e.[(String.length e)-1]
      in Ntsnp( pos, nt2 )
  |_ -> raise UnknownMut

let string_of_ntmut = function
  |Ntsnp(pos,nt) -> (string_of_int pos)^(string_of_nt nt)
  |Ntdel(j1,j2) -> "DEL"
  |Ntins(j,ntl) -> "INS"

let string_of_aamut = function
  |Aasnp(pos,aa) -> (string_of_int pos)^(string_of_aa aa)
  |Aadel(j1,j2) -> "DEL"
  |Aains(j,ntl) -> "INS"

let ntall_of_string line =
  let rec aux buff = function
    |[] -> List.rev buff
    |hd::tl -> 
        let mut = ntmut_of_string hd
        in aux (mut::buff) tl
  in aux [] (String.split_on_char ';' line)


type direction = Left | Up | Diag | Nodir
 
let string_of_array f sep a =
  let (pre,post) = ("[|","|]") in
  let s = Array.fold_left (fun s e -> s^(f e)^sep) pre a in
  let s' = if Array.length a <> 0 then
    String.sub s 0 (String.length s - 1) else s in s'^post

let string_of_direction = function
  |Left -> "Left"
  |Up -> "Up"
  |Diag -> "Diag"
  |Nodir ->"Nodir"

let string_of_dirmat a = string_of_array (string_of_array string_of_direction ",") "\n" a

let needleman_wunsch global score_match score_mismatch score_gap seqA seqB =

  let nA = String.length seqA
  in let nB = String.length seqB

  in let mat_score =
    Array.make_matrix (nA+1) (nB+1) 0

  in let mat_path =
    Array.make_matrix (nA+1) (nB+1) Nodir

  in let fill_in_pos i j =
    if (i = 0 && j = 0) || i > nA || j > nB then ()
    else if i = 0 then 
      begin
        mat_score.(i).(j) <- score_gap + mat_score.(0).(j-1);
        mat_path.(i).(j) <- Left
      end
    else if j = 0 then 
      begin
        mat_score.(i).(j) <- score_gap + mat_score.(i-1).(0);
        mat_path.(i).(j) <- Up
      end
    else
      let nucA = nt_of_char seqA.[i-1]
      in let nucB = nt_of_char seqB.[j-1]
      in let score_diag = 
        if is_overlapping nucA nucB then score_match + mat_score.(i-1).(j-1)
        else score_mismatch + mat_score.(i-1).(j-1)
      in let score_up = score_gap + mat_score.(i-1).(j)
      in let score_left = score_gap + mat_score.(i).(j-1)
      in 
        if score_up > score_left && score_up > score_diag then 
          begin
            mat_score.(i).(j) <- score_up;
            mat_path.(i).(j) <- Up
          end
        else if score_left > score_diag && score_left > score_up then
          begin
            mat_score.(i).(j) <- score_left;
            mat_path.(i).(j) <- Left
          end
        else
          begin
            mat_score.(i).(j) <- score_diag;
            mat_path.(i).(j) <- Diag
          end

  in let _ =
    for i = 0 to nA do
      for j = 0 to nB do
        fill_in_pos i j
      done
    done;

  in let rec find_max_on_edges (max,imax,jmax) = function
    |i,j when i=0 && j=nB -> max,imax,jmax
    |i,j ->
        let newi = if j = nB then i-1 else nA
        in let newj = if j = nB then nB else j+1
        in let max,imax,jmax = 
          if mat_score.(i).(j) > max then mat_score.(i).(j),i,j 
          else max,imax,jmax
        in find_max_on_edges (max,imax,jmax) (newi,newj)

  in let rec build_allele i j prevall =
    if i = 0 && j = 0 then prevall 
    else 
      match mat_path.(i).(j) with
        |Left -> 
          let nucB = nt_of_char seqB.[j-1] in
          let newall =
            match prevall with
            |Ntins (k,ntl)::tl when k = i-1 ->
              let newins = Ntins(i-1, nucB::ntl)
              in newins :: tl
            |_ ->
              Ntins(i-1, nucB::[]) :: prevall
          in build_allele i (j-1) newall
        |Up -> 
          let newall = 
            match prevall with
            |Ntdel(k,j)::tl when k = i -> Ntdel(i-1,j) :: tl
            |_ -> Ntdel(i-1, i-1) :: prevall
          in build_allele (i-1) j newall
        |_ -> 
          let nucB = nt_of_char seqB.[j-1] in
          let nucA = nt_of_char seqA.[i-1]
          in let newall =
            if is_overlapping nucA nucB then prevall
            else Ntsnp(i-1, nucB) :: prevall
          in build_allele (i-1) (j-1) newall 

  in 
    if global then nA, nB, build_allele nA nB []
    else 
      let max,imax,jmax = find_max_on_edges (mat_score.(nA).(0),nA,0) (nA,1)
      in imax, jmax, build_allele imax jmax []

let rec clean_ordered_allele all max_i =
  match all with
  |Ntsnp(i,nt)::tl when i >= max_i -> clean_ordered_allele tl max_i
  |Ntdel(i,j)::tl when i >= max_i -> clean_ordered_allele tl max_i
  |Ntins(i,ntl)::tl when i >= max_i -> clean_ordered_allele tl max_i
  |_ -> all

let shift_all s all = 
  let shift_mut = function
    |Ntsnp(i,nt) -> Ntsnp(i+s,nt)
    |Ntdel(i,j) -> Ntdel(i+s,j+s)
    |Ntins(i,ntl) -> Ntins(i+s,ntl)
  in List.map shift_mut all


let compute_allele refseq seq =

  let rec aux all i shift ichange tol =
  if i < 0 || i + shift < 0 || i >= (String.length refseq) || i + shift >= (String.length seq) then List.rev all
  else
    if is_overlapping (nt_of_char refseq.[i]) (nt_of_char seq.[i+shift]) then 
      aux all (i+1) shift (i+1) tol
    else 
      if (i - ichange + 1) >= tol then 
        raise AlleleWithGap
        (*begin 
          let add_steps_back = if ichange>3 && ichange+shift>3 then 2 else 0 in
          let window_size = 20 in
          let subref = String.sub refseq (ichange-add_steps_back) window_size in
          Printf.printf "subref has been cut from position %i for a length %i\n" (ichange-add_steps_back) window_size;
          let subseq = String.sub seq (ichange+shift-add_steps_back) window_size in
          Printf.printf "seq has been cut from position %i for a length %i\n" (ichange+shift-add_steps_back) window_size;
          let cleaned_all = clean_ordered_allele all (ichange-add_steps_back) in
          let imax, jmax, nw_all = needleman_wunsch false 3 (-5) (-3) subref subseq in
          let shifted_nw_all = shift_all (ichange-add_steps_back) nw_all in
          let new_all = List.rev_append shifted_nw_all cleaned_all in
          let next_i = imax + ichange - add_steps_back in
          let new_shift = shift + jmax - imax in
            aux new_all next_i new_shift next_i tol 
        end*)
      else
        aux (Ntsnp(i, (nt_of_char seq.[i+shift])) :: all) (i+1) shift ichange tol in

  let tol = 3 in
  let start_pos = find_matching_position refseq seq in
  let i = if start_pos >=0 then 0 else -start_pos in
  aux [] i start_pos i tol


let get_snp_list refseq seq shift max_length =

  (* in what follows, i is the index in refseq, i+shift is the index in seq *)
  let rec aux nsnps all i =
    (* if we are outside of one of the sequence, or if we created a list of snps too long *)
    if  i < 0 || i + shift < 0 || 
        i >= (String.length refseq) || 
        i + shift >= (String.length seq) || 
        nsnps >= max_length then 
          List.rev all
    else
      (* look at the next position if this position is similar *)
      if is_overlapping (nt_of_char refseq.[i]) (nt_of_char seq.[i+shift]) then 
        aux nsnps all (i+1)
      (* or add a snp otherwise *)
      else 
        let new_snp_list = Ntsnp(i, (nt_of_char seq.[i+shift])) :: all in
        aux (nsnps+1) new_snp_list (i+1) in

  aux 0 [] 250


