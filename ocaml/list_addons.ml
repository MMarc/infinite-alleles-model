
(** Comparison of two lists [l1] and [l2] already
 * ordered according to [cmp]. The smallest is the one 
 * with the smallest first element, or the shortest.
 * Note that this function appears in Ocaml v > 4.12 as List.compare. *)
let rec compare_list cmp l1 l2 =
  match l1, l2 with
  |[],[] -> 0
  |[],_ -> -1
  |_,[] -> +1
  |e1::tl1, e2::tl2 -> 
      let comparison = cmp e1 e2 in
      if comparison = 0 then compare_list cmp tl1 tl2
      else comparison


let insert_metadata (name, date, region, i) (lname, ldate, lregion, j) =
  (name::lname, date::ldate, region::lregion, i+j)

(** The new list consisting of the [oldlist] within which the element [el] is inserted at the appropriate position.
 * Given that [oldlist] is ordered according to the comparison function [fcompare], the new list will be ordered as well.
 * This function also keeps track of the metadata, as a list associated to each element. *)
let insert_generic fcompare metadata el l =
  let rec aux newlist oldlist =
  (* in this function, oldlist is considered ordered such that the largest elements are at then end.
   * we compare el to elements at the beginning of the oldlist, and put those in newlist. 
   * As a result newlist is ordered with the smallest elements at the end.
   * when we find the appropriate place for el, we put it there and reverse the newlist on top of the remainder of the oldlist.
   * ...which makes the output ordered in the same way as oldlist. *)
  match oldlist with
  |[] -> 
      List.rev ( (el, metadata::[]) :: newlist )
  |(el2, lmetadata)::tl ->
      let comparison = fcompare el el2 in
      if comparison > 0 then
        let newlist = (el2, lmetadata) :: newlist in
        aux newlist tl
      else if comparison = 0 then
        let newlist = (el2, metadata::lmetadata) :: newlist in
        List.rev_append newlist tl
      else
        let newlist = (el, metadata::[]) :: newlist in
        List.rev_append newlist oldlist in
  aux [] l


(** The new list of elements (with associated metadata), 
 * resulting from dispatching all elements found in the list [l1] into an ordered list [l2], in an ordered way. *)
let dispatch_generic fcompare metadata l1 l2 =
  let rec aux l newlist oldlist =
  (* on this function as well, the oldlist is ordered with largest elements at the end.
   * So while reading the beginning of oldlist, we fill the newlist with smallest elements at the end.
   * But when outputing the result, we reverse the newlist. *)
  match oldlist, l with
  |_, [] -> List.rev_append newlist oldlist
  |[], hd::tl -> 
      let newlist = (hd, metadata::[]) :: newlist in
      aux tl newlist oldlist
  |(e2,lmetadata)::tl2, e1::tl1 -> 
      let comparison = fcompare e1 e2 in
      if comparison > 0 then
        let newlist = (e2, lmetadata) :: newlist in
        aux l newlist tl2 
      else if comparison = 0 then
        let newlist =  (e2, (metadata::lmetadata)) :: newlist in
        aux tl1 newlist tl2 
      else
        let newlist = (e1, metadata::[]) :: newlist in
        aux tl1 newlist oldlist in
  aux l1 [] l2



(** An allele (ordered list of snps with largest values at the end) resulting from the union of [all1] and [all2]. *)
let union_list l1 l2 fcompare =
  let rec aux res l1 l2 = 
    match l1, l2 with
    |[], _ -> List.rev_append res l2
    |_, [] -> List.rev_append res l1
    |mut1::tl1, mut2::tl2 ->
        let comparison = fcompare mut1 mut2 in
        if comparison > 0 then
          let newres = mut2 :: res in
          aux newres l1 tl2
        else if comparison = 0 then
          let newres = mut2 :: res in
          aux newres tl1 tl2
        else
          let newres = mut1 :: res in
          aux newres tl1 l2 in
  aux [] l1 l2


(** An allele (ordered list of snps with largest values at the end) resulting from the intersection of the snps in [all1] and [all2]. *)
let intersect_list l1 l2 fcompare =
  let rec aux res l1 l2 =
    match l1, l2 with
    |[], _ -> List.rev res 
    |_, [] -> List.rev res 
    |mut1::tl1, mut2::tl2 ->
        let comparison = fcompare mut1 mut2 in
        if comparison > 0 then
          aux res l1 tl2
        else if comparison = 0 then
          let newres = mut2 :: res in
          aux newres tl1 tl2
        else
          aux res tl1 l2 in
  aux [] l1 l2


(** An allele (ordered list of snps with largest values at the end) resulting from the symmetric difference of the snps in [all1] and [all2] (i.e. keeping only the snps present in one or the other but not both alleles). *)
let symdiff_list l1 l2 fcompare =
  let rec aux res l1 l2 =
    match l1, l2 with
    |[], _ -> List.rev_append res l2
    |_, [] -> List.rev_append res l1
    |mut1::tl1, mut2::tl2 ->
        let comparison = fcompare mut1 mut2 in
        if comparison > 0 then
          let newres = mut2 :: res in
          aux newres l1 tl2
        else if comparison = 0 then
          aux res tl1 tl2
        else
          let newres = mut1 :: res in
          aux newres tl1 l2 in
  aux [] l1 l2



(** A tuple [e, l'] where [e] is the element of the input
 * list [l] number [i], and [l'] is the same list as [l]
 * without the element [e]. *)
let pop i l =
  let rec next_step j lbottom ltop =
    match lbottom with
    |[] -> failwith "index i out of bounds: larger than the size of the list l"
    |hd::tl ->
      if j = 0 then hd, List.rev_append ltop tl
      else next_step (j-1) tl (hd::ltop)
  in next_step i l []


(** The index of the first element e in the list [l]
 * satisfying the predicate [p e] is true. *)
let find_first_index l p =
  let rec aux curr_index = function
    |[] -> failwith "the element has not been found in the list"
    |hd::tl when p hd -> curr_index
    |hd::tl -> aux (curr_index+1) tl in
  aux 0 l


(** The list obtained by removing the first element [el]
 * found in the list [l]. *)
let remove_el el l =
  let rec aux buffer = function
    |[] -> failwith "impossible to remove an element not found in the list"
    |hd::tl ->
        if hd = el then List.rev_append buffer tl
        else aux (hd::buffer) tl in
  aux [] l


(** This function appears in ocaml v > 4.11.
 * The [threading_function] takes as input an accumulator and an element of the list
 * and returns a modified accumulator and element of the list.
 * It is recursively applied from left to right to list [l], starting with accumulator [init]. *)
let fold_left_map threading_function init l = 
  let rec aux acc buffer = function
    |[] -> acc, List.rev buffer
    |hd::tl ->
        let new_acc, new_element = threading_function acc hd in
        let new_buffer = new_element::buffer in
        aux new_acc new_buffer tl in
  aux init [] l


(** The three lists obtained by splitting a list of triples. *)
let split_triple l =
  let rec aux l1 l2 l3 = function
    |[] -> List.rev l1, List.rev l2, List.rev l3
    |(e1,e2,e3)::tl -> aux (e1::l1) (e2::l2) (e3::l3) tl in
  aux [] [] [] l
