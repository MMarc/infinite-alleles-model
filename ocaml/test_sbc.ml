open Coalescent
open Out

let small_SBC_experiment tchange_N tchange_S hyperpriors sampling_params ninit nend =

  let rec run_one_more_experiment = function
    |i when i >= nend -> ()
    |i ->
        let writing_folder = string_of_int i in
        (* we fix the seed for each dataset, in case something goes wrong and we need to test or rerun it *)
        let _ = 
          Random.init i;
          Unix.mkdir writing_folder 0o777 in

        (* simulate the truth according to our model *)
        let listN, listS, mu, g, samp_history, coal_history, alleles = 
          sim_complete_dataset hyperpriors tchange_N tchange_S in
        Printf.printf "g = %f \n" g;

        (* records the truth *)
        let _ =
          begin
            Unix.chdir writing_folder;
            output_hidden_truth listN listS mu g coal_history;
            Unix.chdir "../"
          end in

        (* prepare the first step of the mcmc using only the samp_history and alleles as data *)
        let array_individuals, array_alleles = get_info_arrays samp_history alleles in
        let init_state = gibbs_sampling_init array_individuals array_alleles hyperpriors tchange_N tchange_S in

        (* run the MCMC and sample the posterior distribution *)
        (*let (posterior_all_events, posterior_mu), time_spent = *)
        let _ =
          gibbs_sampling init_state array_individuals array_alleles hyperpriors sampling_params writing_folder true true in

        run_one_more_experiment (i+1) in
  
  let first_dir = Unix.getcwd () in
  begin
    Unix.chdir "../simulation-outputs/test_sbc/";
    run_one_more_experiment ninit;
    Unix.chdir first_dir
  end 

let _ = 
(*
  (* changing 2 *)
  let tchange_N = 0.::30.::[] in
  let tchange_S = 0.::15.::45.::[] in
  let hyperprior_N = (3., 0., 0.01) in
  let hyperprior_S = (2., 2000.) in
  let hyperprior_mu = (2., 200.) in
*)
  (* changing 3 *)
  let tchange_N = 0.::20.::[] in
  let tchange_S = 0.::10.::40.::[] in
  let hyperprior_N = (4., 0., 0.08) in
  let hyperprior_S = (4., 1000.) in
  let hyperprior_mu = (4., 400.) in
  let hyperprior_g = (10., 10.) in
  let tor = infinity in
  let hyperpriors = (hyperprior_N, hyperprior_S, hyperprior_mu, hyperprior_g, tor) in

  (* mcmc sampling parameters *)
  let ninit = 0 in
  let nend = 10000 in
  let nbetween = 50 in
  let sampling_params = (ninit, nend, nbetween) in

  (* SBC dataset numbers that we want to process *)
  let ninit = 9000 in
  let nend = 10000 in

  small_SBC_experiment tchange_N tchange_S hyperpriors sampling_params ninit nend ;;


